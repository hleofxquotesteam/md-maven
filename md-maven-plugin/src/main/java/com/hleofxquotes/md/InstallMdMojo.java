package com.hleofxquotes.md;

import static org.twdata.maven.mojoexecutor.MojoExecutor.artifactId;
import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.executionEnvironment;
import static org.twdata.maven.mojoexecutor.MojoExecutor.goal;
import static org.twdata.maven.mojoexecutor.MojoExecutor.groupId;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;
import static org.twdata.maven.mojoexecutor.MojoExecutor.plugin;
import static org.twdata.maven.mojoexecutor.MojoExecutor.version;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.twdata.maven.mojoexecutor.MojoExecutor;

@Mojo(name = "install", defaultPhase = LifecyclePhase.INSTALL)
public class InstallMdMojo extends AbstractMojo {

	static final String STAGE_DIR_NAME = "md";

	static final String DEFAULT_CACHE_DIR = "download";

	private static final String DEFAULT_PACKAGING = "jar";

	private static final String DEFAULT_GROUP_ID = "com.moneydance.maven";

	/**
	 * The project currently being build.
	 */
	@Parameter(defaultValue = "${project}", readonly = true)
	private MavenProject mavenProject;

	/**
	 * The current Maven session.
	 */
	@Parameter(defaultValue = "${session}", readonly = true)
	private MavenSession mavenSession;

	/**
	 * The Maven BuildPluginManager component.
	 */
	@Component
	private BuildPluginManager pluginManager;

	@Parameter(defaultValue = "${project.build.directory}", readonly = true)
	private String buildDirectory;

	private String groupId = DEFAULT_GROUP_ID;

	private String packaging = DEFAULT_PACKAGING;

	private String cacheDirName = InstallMdMojo.DEFAULT_CACHE_DIR;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("> execute");
		getLog().info(">   buildDirectory=" + buildDirectory);

		try {
			// 2020.1 (1928) -> 2020.1.1928
			String version = MdVersionMojo.readVersion(cacheDirName, MdVersionMojo.MD_VERSION_TXT, true);
			getLog().info("version=" + version);

			// if we just downloaded a new version: there is a stage dir
			File stageDir = new File(String.format(cacheDirName + "/%s", InstallMdMojo.STAGE_DIR_NAME));

//            String dirName = String.format(cacheDirName + "/%s", version);
			File versionedDir = new File(String.format(cacheDirName + "/%s", version));

			String currentVersionFn = MdVersionMojo.MY_VERSION_TXT;
			if (stageDir.exists()) {
				// Move 'md' -> 2020.1.1928
				getLog().info("YES stageDir=" + stageDir.getAbsolutePath());
				// rename to version
				if (stageDir.renameTo(versionedDir)) {
					// then update the md_version.txt file
					Files.write(Path.of(cacheDirName, currentVersionFn), version.getBytes());
				}
			} else {
				getLog().info("NO stageDir=" + stageDir.getAbsolutePath());

				// Make sure we have md_version.txt
				if (!Path.of(cacheDirName, currentVersionFn).toFile().exists()) {
					Files.write(Path.of(cacheDirName, currentVersionFn), version.getBytes());
				}
				version = MdVersionMojo.readVersion(cacheDirName, currentVersionFn, false);
			}

			String libDirName = String.format(cacheDirName + "/%s/Moneydance/lib/", version);
			File libDir = new File(libDirName);
			if (!libDir.exists()) {
				throw new MojoFailureException("Cannot find directory=" + libDirName);
			}
			File[] jarFiles = libDir.listFiles();

			processJarFiles(version, jarFiles);

		} catch (IOException e) {
			throw new MojoFailureException(e.getMessage(), e);
		}
	}

	private void processJarFiles(String version, File[] files) throws MojoExecutionException, IOException {
		File parentDir = new File(buildDirectory);
		File parentPomFile = new File(parentDir, "parent-pom.xml");

		try (PrintWriter parentPomWriter = new PrintWriter(new FileWriter(parentPomFile))) {
			writeParentPomWriterHeader(version, groupId, parentPomWriter);

			for (File file : files) {
				processJarFile(file, version, parentPomWriter);
			}

			writeParentPomWriterFooter(parentPomWriter);
		}

		installParentPom(parentPomFile.getAbsolutePath());

		getLog().info("***");
		getLog().info("*** Installed MD version=" + version);
		getLog().info("***");
	}

	private static final void writeParentPomWriterFooter(PrintWriter writer) {
		writer.println("</dependencies>");
		writer.println("</project>");
	}

	private static final void writeParentPomWriterHeader(String version, String groupId, PrintWriter writer) {
		writer.println(
				"<project xmlns='http://maven.apache.org/POM/4.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'");
		writer.println(
				"xsi:schemaLocation='http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd'>");
		writer.println("<modelVersion>4.0.0</modelVersion>");
		writer.println(String.format("<groupId>%s</groupId>", groupId));
		writer.println("<artifactId>md-parent</artifactId>");
		writer.println("<packaging>pom</packaging>");
		writer.println(String.format("<version>%s</version>", version));
		writer.println("<name>md-parent</name>");

		writer.println("<dependencies>");
	}

	private void processJarFile(File file, String version, PrintWriter parentPomWriter) throws MojoExecutionException {
		String fn = file.getName();

		String artifactId = fn;
		int index = fn.lastIndexOf('.');
		if (index > 0) {
			artifactId = fn.substring(0, index);
		}

		String artifactFile = String.format(cacheDirName + "/%s/Moneydance/lib/%s", version, fn);
		installJarFile(artifactFile, groupId, artifactId, version, packaging);

		parentPomWriter.println("  <dependency>");
		parentPomWriter.println(String.format("    <groupId>%s</groupId>", groupId));
		parentPomWriter.println(String.format("    <artifactId>%s</artifactId>", artifactId));
		parentPomWriter.println(String.format("    <version>%s</version>", version));
		parentPomWriter.println("  </dependency>");
	}

	private void installParentPom(String file) throws MojoExecutionException {
//        <configuration>
//        <file>parent-pom.xml</file>
//        <pomFile>parent-pom.xml</pomFile>
//      </configuration>
//      
		MojoExecutor.executeMojo(
				plugin(groupId("org.apache.maven.plugins"), artifactId("maven-install-plugin"), version("2.5.2")),
				goal("install-file"), configuration(element(name("file"), file), element(name("pomFile"), file)),
				executionEnvironment(mavenProject, mavenSession, pluginManager));
	}

	private void installJarFile(String file, String groupId, String artifactId, String version, String packaging)
			throws MojoExecutionException {
		getLog().debug("file=" + file);
		getLog().debug("  groupId=" + groupId);
		getLog().debug("  artifactId=" + artifactId);
		getLog().debug("  version=" + version);
		getLog().debug("  packaging=" + packaging);

//                <groupId>org.apache.maven.plugins</groupId>
//                <artifactId>maven-install-plugin</artifactId>
//                <version>2.5.2</version>

		MojoExecutor.executeMojo(
				plugin(groupId("org.apache.maven.plugins"), artifactId("maven-install-plugin"), version("2.5.2")),
				goal("install-file"),
				configuration(element(name("file"), file), element(name("groupId"), groupId),
						element(name("artifactId"), artifactId), element(name("version"), version),
						element(name("packaging"), packaging)),
				executionEnvironment(mavenProject, mavenSession, pluginManager));
	}

}
