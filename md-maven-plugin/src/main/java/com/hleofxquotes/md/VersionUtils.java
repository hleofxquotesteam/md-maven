package com.hleofxquotes.md;

import org.apache.commons.lang3.StringUtils;

public class VersionUtils {
	public static final String getUrl(String prefix, String type, String version) {
		// https://infinitekind.com/stabledl/2020.1_1928/

		if (StringUtils.isAllBlank(prefix)) {
			prefix = "https://infinitekind.com/";
		}
		if (StringUtils.isAllBlank(type)) {
			prefix = "stabledl";
		}
		if (StringUtils.isAllBlank(version)) {
			version = "current";
		}

		return prefix + "/" + type + "/" + version;
	}
}
