package com.hleofxquotes.md;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "version", defaultPhase = LifecyclePhase.INSTALL)
public class MdVersionMojo extends AbstractMojo {
	static final String MD_VERSION_TXT = "version.txt";

	static final String MY_VERSION_TXT = "md_version.txt";

    /**
	 * The project currently being build.
	 */
	@Parameter(defaultValue = "${project}", readonly = true)
	private MavenProject mavenProject;

	/**
	 * The current Maven session.
	 */
	@Parameter(defaultValue = "${session}", readonly = true)
	private MavenSession mavenSession;

	/**
	 * The Maven BuildPluginManager component.
	 */
	@Component
	private BuildPluginManager pluginManager;

	@Parameter(defaultValue = "${project.build.directory}", readonly = true)
	private String buildDirectory;

	private String cacheDirName = InstallMdMojo.DEFAULT_CACHE_DIR;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("> execute");
		getLog().info(">   buildDirectory=" + buildDirectory);

		try {
			String version = MdVersionMojo.readVersion(cacheDirName, MdVersionMojo.MD_VERSION_TXT, true);
			getLog().info("version=" + version);

			// if we just downloaded a new version: there is a stage dir
			File stageDir = new File(String.format(cacheDirName + "/%s", InstallMdMojo.STAGE_DIR_NAME));

//            String dirName = String.format(cacheDirName + "/%s", version);
			File versionedDir = new File(String.format(cacheDirName + "/%s", version));

			String currentVersionFn = MdVersionMojo.MY_VERSION_TXT;

			if (stageDir.exists()) {
				// Move 'md' -> 2020.1.1928
				getLog().info("YES stageDir=" + stageDir.getAbsolutePath());
			} else {
				getLog().info("NO stageDir=" + stageDir.getAbsolutePath());
			}

			if (versionedDir.exists()) {
				// Move 'md' -> 2020.1.1928
				getLog().info("YES versionedDir=" + versionedDir.getAbsolutePath());
			} else {
				getLog().info("NO versionedDir=" + versionedDir.getAbsolutePath());
			}

			File currentVersionFile = Path.of(cacheDirName, currentVersionFn).toFile();
			if (currentVersionFile.exists()) {
				getLog().info("YES currentVersionFile=" + currentVersionFile.getAbsolutePath());
			} else {
				getLog().info("NO currentVersionFile=" + currentVersionFile.getAbsolutePath());
			}

		} catch (IOException e) {
			throw new MojoFailureException(e.getMessage(), e);
		}
	}

	static final String readVersion(String dirName, String fileName, boolean clean)
			throws IOException, MojoFailureException {
		String version = Files.readString(Path.of(dirName, fileName));
		if (StringUtils.isBlank(version)) {
			throw new MojoFailureException("version is blank.");
		}
		if (clean) {
			// 2020 (1917)
			version = version.replace(' ', '.').replaceAll("[\\(\\)]", "");
		}
		return version.strip();
	}

}
