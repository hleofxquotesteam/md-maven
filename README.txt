Minimum set of steps needed to mavenize the Moneydance libs

The goal is to have a parent POM file so that in your project you can just do

  <dependencies>
...
    <dependency>
      <groupId>com.moneydance.maven</groupId>
      <artifactId>md-parent</artifactId>
      <version>${md_version}</version>
      <type>pom</type>
    </dependency>
  </dependencies>

and it will simply work.

Prerequisite
  * Maven installed (https://maven.apache.org/)

What you need to do
  * Download the zip file: md-maven-1.0-SNAPSHOT.zip
  * Unzip the above file to get directory 'md-maven-install'
  * Go to directory 'md-maven-install'
  * Run 
mvn install
  * That will attemp to down the current MD preview (https://infinitekind.com/previewdl/current/Moneydance_linux_amd64.tar.gz
  * then create some Maven-specific meta-data so that all jar files can be used as a dependencies
  * then install those dependencies on your system

Now try a simple Maven project, 

  * Add to your project POM
  <dependencies>
...
    <dependency>
      <groupId>com.moneydance.maven</groupId>
      <artifactId>md-parent</artifactId>
      <version>${md_version}</version>
      <type>pom</type>
    </dependency>
  </dependencies>

  * Create a class that has reference to MD class. Something like this

package com.xyz.java.md;

import com.moneydance.apps.md.controller.Main;

public class MdMain extends com.moneydance.apps.md.controller.Main {
    public static void main(String[] args) {
        Main.DEBUG = true;
    }
}

