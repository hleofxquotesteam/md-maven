# download
mvn antrun:run@download-files

# check version
mvn com.hleofxquotes.md:md-maven-plugin:version

# remove 
mvn dependency:purge-local-repository -DmanualInclude=com.moneydance.maven

# install
# to install specific version download/md_version.txt 
mvn com.hleofxquotes.md:md-maven-plugin:install

