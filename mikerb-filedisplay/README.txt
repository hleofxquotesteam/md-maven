# Download MD distribution

wget -N https://infinitekind.com/previewdl/current/Moneydance_linux_amd64.tar.gz

# Extract to get directory Moneydance/lib/

tar xvf Moneydance_linux_amd64.tar.gz

# Make directory libs

mkdir libs

# Download current filedisplay extension

wget https://bitbucket.org/mikerb/moneydance-2019/downloads/filedisplay.mxt

# Rename and move it to libs/

mv filedisplay.mxt libs/filedisplay.jar

Start
./run.sh path/to/md/folder [password]
For example
./run.sh /a/b//moneyDance/MsMoneySample.moneydance 

# Running python script instead of Mike's file display
./run.sh path/to/md/folder [password] [script.py]

## Error

Exception in thread "main" java.lang.NoClassDefFoundError: com/moneydance/apps/md/controller/FeatureModuleContext
	at java.base/java.lang.Class.getDeclaredMethods0(Native Method)
	at java.base/java.lang.Class.privateGetDeclaredMethods(Class.java:3166)
	at java.base/java.lang.Class.getDeclaredMethod(Class.java:2473)

That means you don't have libs/filedisplay.jar  

## Error
Exception in thread "main" java.lang.NoClassDefFoundError: com/moneydance/apps/md/controller/FeatureModuleContext
	at java.base/java.lang.Class.getDeclaredMethods0(Native Method)
	at java.base/java.lang.Class.privateGetDeclaredMethods(Class.java:3166)
	at java.base/java.lang.Class.getDeclaredMethod(Class.java:2473)

That means you don't have Moneydance/lib/

## Error
com.hleofxquotes.md.io.MdFileOpenException: java.lang.IllegalStateException: Attempting to load Moneydance data model before local storage initialised
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:197)
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:215)
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:252)
	at com.moneydance.modules.features.filedisplay.StandaloneMain.main(StandaloneMain.java:37)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:566)
	at org.springframework.boot.loader.MainMethodRunner.run(MainMethodRunner.java:49)
	at org.springframework.boot.loader.Launcher.launch(Launcher.java:109)
	at org.springframework.boot.loader.Launcher.launch(Launcher.java:58)
	at org.springframework.boot.loader.PropertiesLauncher.main(PropertiesLauncher.java:466)
Caused by: java.lang.IllegalStateException: Attempting to load Moneydance data model before local storage initialised
	at com.moneydance.apps.md.controller.AccountBookWrapper.loadDataModel(AccountBookWrapper.java:327)
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:189)

That means you try to open password-protected file without specifying a password.

## Error
com.hleofxquotes.md.io.MdFileOpenException: wrong_encryption_password_or_data_version
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:195)
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:254)
	at com.moneydance.modules.features.filedisplay.StandaloneMain.main(StandaloneMain.java:37)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:566)
	at org.springframework.boot.loader.MainMethodRunner.run(MainMethodRunner.java:49)
	at org.springframework.boot.loader.Launcher.launch(Launcher.java:109)
	at org.springframework.boot.loader.Launcher.launch(Launcher.java:58)
	at org.springframework.boot.loader.PropertiesLauncher.main(PropertiesLauncher.java:466)
Caused by: com.moneydance.apps.md.controller.MDException
	at com.moneydance.apps.md.controller.AccountBookWrapper.loadLocalStorage(AccountBookWrapper.java:265)
	at com.hleofxquotes.md.io.AccountBookWrapperUtils.open(AccountBookWrapperUtils.java:188)
	... 10 more

That means you try to open password-protected file with a incorrect password.
