package com.moneydance.modules.features.filedisplay;

import java.io.File;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.python.util.PythonInterpreter;

import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.hleofxquotes.md.io.DefaultFeatureModuleContext;
import com.hleofxquotes.md.io.MdFileOpenException;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class StandaloneMain {
    private static final Log LOGGER = LogFactory.getLog(StandaloneMain.class);

    public static void main(String[] args) {
        String folderName = null;
        String passPhrase = null;
        String pythonScript = null;
        
        if (args.length == 1) {
            folderName = args[0];
        } else if (args.length == 2) {
            folderName = args[0];
            passPhrase = args[1];
            if (passPhrase.endsWith(".py")) {
                pythonScript = passPhrase;
                passPhrase = null;
            }
        } else if (args.length == 3) {
            folderName = args[0];
            passPhrase = args[1];
            pythonScript = args[2];
        } else {
            Class<StandaloneMain> clz = StandaloneMain.class;
            System.out.println("Usage: java " + clz.getName() + " mdFolder [passPhrase] [script.py]");
            System.exit(1);
        }

        LOGGER.info("folderName=" + folderName);
//        LOGGER.info("passPhrase=" + ((passPhrase == null) ? passPhrase : passPhrase));
        LOGGER.info("pythonScript=" + pythonScript);
        
        File file = new File(folderName);
        if (! file.isDirectory()) {
            LOGGER.error("Not a directory=" + file.getAbsolutePath());
            System.exit(1);
        }
        
        try {
            com.moneydance.apps.md.controller.Main.DEBUG = true;
            com.moneydance.apps.md.controller.Main mdMain = new com.moneydance.apps.md.controller.Main();
            mdMain.initializeApp();
            
            AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(folderName, passPhrase);
//            mdMain.setCurrentBook(accountBookWrapper);
            
            if (pythonScript != null) {
                PythonInterpreter python = mdMain.getPythonInterpreter();
                python.set("moneydance_data", accountBookWrapper.getBook());
                python.execfile(pythonScript);
            } else {
                StandaloneMain.showMainFrame(accountBookWrapper);
            }

        } catch (MdFileOpenException e) {
            LOGGER.error("Cannot open file=" + folderName, e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private static void showMainFrame(AccountBookWrapper accountBookWrapper) {
        Main.context = new DefaultFeatureModuleContext(accountBookWrapper);
        Main moduleMain = new Main() {
            @Override
            synchronized void closeConsole() {
                LOGGER.info("> closeConsole");
                System.exit(0);
            }
        };

        final FileDisplayWindow mainFrame = new FileDisplayWindow(moduleMain) {
        };
        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//                    mainFrame.setResizable(true);

//                    mainFrame.pack();
                mainFrame.setLocationRelativeTo(null);
                mainFrame.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

}
