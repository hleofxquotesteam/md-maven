from com.infinitekind.moneydance.model import *

import sys
import time

# get the default environment variables, set by Moneydance
print "The Moneydance app controller: %s"%(moneydance)
print "The current data set: %s"%(moneydance_data)
print "The UI: %s"%(moneydance_ui)

if moneydance_data:
  txnSet = moneydance_data.getTransactionSet()
  
  counter = 0
  for txn in txnSet.iterableTxns():
    if counter < 10:
      print "transaction: date %u: description: %s for amount %s"%\
          (txn.getDateInt(), txn.getDescription(), txn.getAccount().getCurrencyType().formatFancy(txn.getValue(), '.'))
    counter += 1
