package com.moneydance.modules.features.reportwriter;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.hleofxquotes.md.io.DefaultFeatureModuleContext;
import com.hleofxquotes.md.io.MdFileOpenException;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class StandaloneMain {
    private static final Log LOGGER = LogFactory.getLog(StandaloneMain.class);

    public static void main(String[] args) {
        String folderName = null;
        String passPhrase = null;

        if (args.length == 1) {
            folderName = args[0];
        } else if (args.length == 2) {
            folderName = args[0];
            passPhrase = args[1];
        } else {
            Class<StandaloneMain> clz = StandaloneMain.class;
            System.out.println("Usage: java " + clz.getName() + " mdFolder [passPhrase]");
            System.exit(1);
        }

        try {
            AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(folderName, passPhrase);

            Main.context = new DefaultFeatureModuleContext(accountBookWrapper);
            Main main = new Main() {
                @Override
                synchronized void closeConsole() {
                    LOGGER.info("> closeConsole");
                    System.exit(0);
                }

            };
//            final FileDisplayWindow mainFrame = new FileDisplayWindow(main) {
//            };
//            Runnable doRun = new Runnable() {
//                @Override
//                public void run() {
//                    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
////                    mainFrame.setResizable(true);
//
////                    mainFrame.pack();
//                    mainFrame.setLocationRelativeTo(null);
//                    mainFrame.setVisible(true);
//                }
//            };
//            SwingUtilities.invokeLater(doRun);

        } catch (MdFileOpenException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
