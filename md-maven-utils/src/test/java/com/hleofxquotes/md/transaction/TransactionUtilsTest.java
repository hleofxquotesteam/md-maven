package com.hleofxquotes.md.transaction;

import java.io.IOException;
import java.util.stream.Stream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.OnlineTxn;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class TransactionUtilsTest {
    private Log log = LogFactory.getLog(TransactionUtilsTest.class);

    @Test
    public void testGetStream() throws IOException {
        String folderName = "src/test/resources/My_Wealth.moneydance";
        final AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(folderName);
        final AccountBook book = accountBookWrapper.getBook();

        Stream<AbstractTxn> stream = TransactionUtils.getTransactionStream(book);
        stream.filter(t -> {
            return true;
        }).forEach(t -> {
            int protocolId = OnlineTxn.PROTO_TYPE_OFX;
            String fiTxnId = t.getFiTxnId(protocolId);
            log.info(t.toString());
        });

    }
}
