package com.hleofxquotes.md.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hleofxquotes.md.account.AccountUtils;
import com.hleofxquotes.md.category.CategoryUtils;
import com.hleofxquotes.md.transaction.TransactionSplitInfo;
import com.hleofxquotes.md.transaction.TransactionUtils;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.moneydance.model.AccountUtil;
import com.infinitekind.moneydance.model.AcctFilter;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.moneydance.model.TransactionSet;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class AccountBookWrapperUtilsTest {
    private Log log = LogFactory.getLog(AccountBookWrapperUtilsTest.class);

    public static final String TARGET_TEST_MONEYDANCE = "target/test.moneydance";

    private com.moneydance.apps.md.controller.Main main;

    @Before
    public void before() throws Exception {
        main = new com.moneydance.apps.md.controller.Main();
        main.initializeApp();
    }

    @Test
    public void testOpenWithPassPhrase() throws MdFileOpenException {
        String folderName = "src/test/resources/MsMoneySample_password.moneydance";
        String passPhrase = "123@abc!";

        AccountBookWrapper wrapper = AccountBookWrapperUtils.open(folderName, passPhrase);
        Assert.assertNotNull(wrapper);
    }

    @Test
    public void testCreateAndOpen() throws IOException {
        File folder = new File(AccountBookWrapperUtilsTest.TARGET_TEST_MONEYDANCE);
        AccountBookWrapperUtils.deleteFolder(folder);

        AccountBookWrapper bookWrapper = null;
        AccountBook book = null;

        // create
        bookWrapper = AccountBookWrapperUtils.createWithStandardAccounts(folder);
        book = bookWrapper.getBook();
        Assert.assertTrue(book.isValid());

        // open
        bookWrapper = AccountBookWrapperUtils.openFolder(folder);
        book = bookWrapper.getBook();
        Assert.assertTrue(book.isValid());

        // standard accounts and categories are loaded.
        checkAccounts(bookWrapper);
        checkCategories(bookWrapper);
        checkTransactions(bookWrapper);

        try {
            createTestAccounts(bookWrapper);
            createTransactions(bookWrapper);
        } finally {
            book.save();
            Assert.assertTrue(book.isValid());
        }

        checkGetStreams(book);
    }

    private void checkGetStreams(AccountBook book) {
        AccountUtils.getAccountStream(book).forEach(acct -> {
            log.info("acct=" + acct.toString());
        });

        AccountUtils.getCurrencyTypeStream(book).forEach(cType -> {
            log.info("cType=" + cType.toString());
        });

        TransactionUtils.getTransactionStream(book).forEach(txn -> {
            log.info("accountName=" + txn.getAccount().getAccountName() + ", uuid=" + txn.getUUID() + ", txn="
                    + txn.toString());
        });
    }

    private void createTestAccounts(AccountBookWrapper bookWrapper) {
        final AccountBook accountBook = bookWrapper.getBook();

        for (AccountType accountType : Account.AccountType.values()) {
            if ((accountType == AccountType.ROOT) || (accountType == AccountType.SECURITY)) {
                continue;
            }
            String accountName = accountType + "-" + UUID.randomUUID().toString();
            Account account = AccountUtils.createAccount(accountBook, accountName, accountType);
            if (account != null) {
                account.ensureAccountStructure();
                account.syncItem();
            } else {
                log.warn("Cannot create account for accountType=" + accountType);
            }
        }
    }

    private void createTransactions(AccountBookWrapper bookWrapper) {
        final AccountBook book = bookWrapper.getBook();
        final Account root = book.getRootAccount();

        for (Account account : root.getSubAccounts()) {
            Assert.assertNotNull(account);
            AccountType accountType = account.getAccountType();
            Assert.assertNotNull(accountType);

            log.info("accountType=" + accountType + ", account.balanceIsNegated=" + account.balanceIsNegated());

            switch (accountType) {
            case ASSET:
                break;
            case BANK:
                createBankTransactions(book, account);
                break;
            case CREDIT_CARD:
                createCreditCardTransactions(book, account);
                break;
            case EXPENSE:
                // category
                break;
            case INCOME:
                // category
                break;
            case INVESTMENT:
                break;
            case LIABILITY:
                break;
            case LOAN:
                break;
            case ROOT:
                // root
                break;
            case SECURITY:
                break;
            }
        }
    }

    private void createCreditCardTransactions(final AccountBook book, final Account account) {
        // Bills:Cable
        Account expenseCategory = TransactionUtils.getCategory(book, account, "Bills:Cable");
        TransactionUtils.addTransaction(book, account, -12345L, expenseCategory, "Xfinity");

        AcctFilter search = new AcctFilter() {

            @Override
            public boolean matches(Account paramAccount) {
                AccountType type = paramAccount.getAccountType();
                if (type == AccountType.BANK) {
                    return true;
                }

                return false;
            }

            @Override
            public String format(Account paramAccount) {
                return paramAccount.getFullAccountName();
            }
        };
        // payment
        Account xferAccount = AccountUtil.firstMatchForSearch(book.getRootAccount(), search);
        TransactionUtils.addTransaction(book, account, 12345L, xferAccount, "Transfer");
    }

    private void createBankTransactions(final AccountBook book, final Account account) {
        Account incomeCategory = TransactionUtils.getCategory(book, account, "Investment Income");
        TransactionUtils.addTransaction(book, account, 11100L, incomeCategory, "Payee001");

        Account expenseCategory = TransactionUtils.getCategory(book, account, "Bills:Phone");
        TransactionUtils.addTransaction(book, account, -999L, expenseCategory, "Payee002");

        List<TransactionSplitInfo> splits = new ArrayList<>();
        TransactionSplitInfo split = null;

        split = new TransactionSplitInfo();
        split.setAmount(1100L);
        split.setCategory(incomeCategory);
        split.setPayeeName("Payee03");
        splits.add(split);

        split = new TransactionSplitInfo();
        split.setAmount(2200L);
        split.setCategory(incomeCategory);
        split.setPayeeName("Payee03");
        splits.add(split);

        split = new TransactionSplitInfo();
        split.setAmount(3300L);
        split.setCategory(incomeCategory);
        split.setPayeeName("Payee03");
        splits.add(split);

        TransactionUtils.addTransaction(book, account, splits);
    }

    private void checkAccounts(AccountBookWrapper bookWrapper) {
        int rootCounter = 0;
        int accountCounter = 0;
        int securityCounter = 0;
        int categoryCounter = 0;
        int unknownCounter = 0;
        AccountIterator accountIterator = new AccountIterator(bookWrapper.getBook());
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();
            if (AccountUtils.isRoot(account)) {
                log.info("root=" + account);
                rootCounter++;
            } else if (AccountUtils.isAccount(account)) {
                log.info("account=" + account);
                accountCounter++;
            } else if (AccountUtils.isSecurity(account)) {
                log.info("security=" + account);
                securityCounter++;
            } else if (AccountUtils.isCategory(account)) {
                log.info("category=" + account);
                categoryCounter++;
            } else {
                log.info("unknown=" + account);
                unknownCounter++;
            }
        }

        Assert.assertEquals(1, rootCounter);
        Assert.assertEquals(2, accountCounter);
        Assert.assertEquals(0, securityCounter);
        Assert.assertEquals(108, categoryCounter);
        Assert.assertEquals(0, unknownCounter);

        Account root = bookWrapper.getBook().getRootAccount();
        Assert.assertNotNull(root.getAccountByName("ATM Withdrawal"));
        Assert.assertNotNull(root.getAccountByName("ATM Withdrawal:Cash"));
        Assert.assertNull(root.getAccountByName("XATM Withdrawal:Cash"));
        Assert.assertNull(root.getAccountByName("ATM Withdrawal:XCash"));
    }

    private void checkCategories(AccountBookWrapper bookWrapper) {
        List<Account> categories = CategoryUtils.getCategories(bookWrapper);
        Assert.assertNotNull(categories);
        Assert.assertEquals(108, categories.size());

        for (Account category : categories) {
            checkCategory(bookWrapper, category);
        }
    }

    private void checkCategory(AccountBookWrapper bookWrapper, Account category) {
        String categoryName = category.toString();
        Account found = CategoryUtils.findCategory(bookWrapper, categoryName);
        Assert.assertNotNull(found);
        Assert.assertTrue(AccountUtils.isCategory(category));

        found = CategoryUtils.findCategory(bookWrapper, categoryName + UUID.randomUUID().toString());
        Assert.assertNull(found);
    }

    private void checkTransactions(AccountBookWrapper bookWrapper) {
        TransactionSet transactions = bookWrapper.getBook().getTransactionSet();
        log.info("getAllTxns.getSize()=" + transactions.getAllTxns().getSize());

        for (AbstractTxn transaction : transactions) {
            checkTransaction(transaction);
        }
    }

    private void checkTransaction(AbstractTxn transaction) {
        int parentTxnCount = 0;
        int splitTxnCount = 0;
        if (transaction instanceof ParentTxn) {
            ParentTxn parentTxn = (ParentTxn) transaction;
            checkParentTxn(parentTxn);
            parentTxnCount++;
        } else if (transaction instanceof SplitTxn) {
            SplitTxn splitTxn = (SplitTxn) transaction;
            checkSplitTxn(splitTxn);
            splitTxnCount++;
        } else {
            Assert.assertFalse("Unknown transaction.class=" + transaction.getClass().getName(), true);
        }
        log.info("parentTxnCount=" + parentTxnCount + ", " + "splitTxnCount=" + splitTxnCount);
    }

    private void checkSplitTxn(SplitTxn splitTxn) {
        // TODO Auto-generated method stub

    }

    private void checkParentTxn(ParentTxn parentTxn) {
        // TODO Auto-generated method stub

    }

}
