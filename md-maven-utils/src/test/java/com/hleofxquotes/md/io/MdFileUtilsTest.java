package com.hleofxquotes.md.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.hleofxquotes.md.account.AccountUtils;
import com.hleofxquotes.md.category.AbstractCategories;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AcctFilter;
import com.moneydance.apps.md.controller.AccountBookWrapper;

// TODO: Auto-generated Javadoc
/**
 * The Class MdFileUtilsTest.
 */
public class MdFileUtilsTest {

    /** The log. */
    private Log log = LogFactory.getLog(MdFileUtilsTest.class);

    /**
     * Test create account book wrapper.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testCreateAccountBookWrapper() throws IOException {
        String prefix = "md";
        TempDirectory tempDirectory = new TempDirectory(prefix);
        tempDirectory.deleteOnExit();

        Assert.assertNotNull(tempDirectory);
        Path path = tempDirectory.getPath();
        File dir = path.toFile();
        Assert.assertNotNull(dir);
        Assert.assertTrue(dir.exists());
        Assert.assertTrue(dir.isDirectory());

        AccountBookWrapper bookWrapper = AccountBookWrapperUtils.createWithBasicAccounts(dir);
        Assert.assertNotNull(bookWrapper);

        MdFileUtils.checkBookWrapper(bookWrapper, false);

        for (Account.AccountType accountType : Account.AccountType.values()) {
            if (!AccountUtils.isAccount(accountType)) {
                continue;
            }
            String accountName = accountType.toString() + "-" + UUID.randomUUID().toString();
//            log.info("accountName=" + accountName + ", type=" + accountType);

            Account account = AccountUtils.createAccount(bookWrapper.getBook(), accountName, accountType);
            Assert.assertNotNull(account);
        }

        final AccountBook book = bookWrapper.getBook();
        AbstractCategories categories = new AbstractCategories() {
            @Override
            protected void visit(final String parentName, String name, String type, String currencyType,
                    String taxCategory) {
                String fullName = null;
                
                if (parentName != null) {
                    fullName = parentName + ":" + name;
                } else {
                    fullName = name;
                }
                log.info("CATEGORIES " + fullName);

                Account category = null;
                
                if (parentName == null) {
                    Account.AccountType accountType = Account.AccountType.valueOf(type);
                    category = Account.makeAccount(book, accountType, book.getRootAccount());
                } else {
                    AcctFilter search = new AcctFilter() {
                        @Override
                        public boolean matches(Account account) {
                            return account.getAccountName().compareToIgnoreCase(parentName) == 0;
                        }

                        @Override
                        public String format(Account account) {
                            return account.getAccountName();
                        }
                    };
                    final List<Account> subAccounts = book.getRootAccount().getSubAccounts(search);
                    if ((subAccounts == null) || (subAccounts.size() <= 0)) {
                        log.warn("Cannot find parent account, name=" + parentName);
                    } else {
                        Account parentAccount = subAccounts.get(0);
                        Account.AccountType accountType = parentAccount.getAccountType();
                        category = Account.makeAccount(book, accountType, parentAccount);
                    }
                }
                
//                Assert.assertNotNull(category);

            }
        };
        categories.loadDefaultCategories();

        // AccountBook book, int date, int taxDate, long dateEntered, String
        // checkNumber, Account account, String description, String memo, long id, byte
        // status

//        Date now = new Date();
//        int date = DateUtil.convertDateToInt(now);
//        int taxDate = date;
//        String checkNumber = "checkNumber";
//        Account account = null;
//        String description = "description";
//        String memo = "memo";
//        byte status = AbstractTxn.STATUS_UNRECONCILED;
//        final int id = -1;
//        ParentTxn parentTxn = ParentTxn.makeParentTxn(book,
//                date,
//                taxDate,
//                System.currentTimeMillis(),
//                checkNumber,
//                account,
//                description,
//                memo,
//                id,
//                status);
//        long parentAmount = 100;
//        Account category = null;
//        // ParentTxn parentTxn, long parentAmount, double rate, Account account, String description, long txnId, byte status) {
//        parentTxn.addSplit(SplitTxn.makeSplitTxn(parentTxn,
//                parentAmount,
//                1.0,
//                category,
//                parentTxn.getDescription(),
//                -1,
//                status));
//        parentTxn.syncItem();
//
//        MdFileUtils.checkBookWrapper(bookWrapper, true);
    }
}
