package com.hleofxquotes.md.io;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.hleofxquotes.md.account.AccountUtils;
import com.hleofxquotes.md.category.CategoryUtils;
import com.hleofxquotes.md.transaction.TransactionUtils;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;
import com.infinitekind.moneydance.model.TransactionSet;
import com.moneydance.apps.md.controller.AccountBookWrapper;

/**
 * The Class OpenFileTest.
 */
public class OpenFileTest {

    /** The log. */
    public Log log = LogFactory.getLog(OpenFileTest.class);

    /**
     * Test create from sample.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCreateFromSample() throws Exception {
        String prefix = "md-";
        TempDirectory tempDirectory = new TempDirectory(prefix);
        tempDirectory.deleteOnExit();
        Path destPath = tempDirectory.getPath();

        File folder = SampleFile.createSampleFile(destPath);
        testOpen(folder.getAbsolutePath());
    }

    /**
     * Test create accounts.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCreateAccounts() throws Exception {
        final File folder = SampleFile.createSampleFolderWithData();

        testOpen(folder.getAbsolutePath());
    }

    /**
     * Test open an existing moneydance file.
     *
     * @throws Exception the exception
     */
    @Test
    public void testOpen() throws Exception {
        String[] folderNames = { "src/test/resources/My_Wealth.moneydance", };

        for (String folderName : folderNames) {
            testOpen(folderName);
        }
    }

    @Test
    public void testAccountiterator() throws Exception {
        String folderName = "src/test/resources/My_Wealth.moneydance";
        final AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(folderName);
        Assert.assertNotNull(accountBookWrapper);

        int rootCounter = 0;
        int accountCounter = 0;
        int securityCounter = 0;
        int categoryCounter = 0;
        int unknownCounter = 0;

        AccountIterator accountIterator = new AccountIterator(accountBookWrapper.getBook());
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();
            if (AccountUtils.isRoot(account)) {
                log.info("root=" + account);
                rootCounter++;
            } else if (AccountUtils.isAccount(account)) {
                log.info("account=" + account);
                accountCounter++;
            } else if (AccountUtils.isSecurity(account)) {
                log.info("security=" + account);
                securityCounter++;
            } else if (AccountUtils.isCategory(account)) {
                log.info("category=" + account);
                categoryCounter++;
            } else {
                log.info("unknown=" + account);
                unknownCounter++;
            }
        }

        Assert.assertEquals(1, rootCounter);
        Assert.assertEquals(2, accountCounter);
        Assert.assertEquals(0, securityCounter);
        Assert.assertEquals(108, categoryCounter);
        Assert.assertEquals(0, unknownCounter);

        CurrencyTable currencies = accountBookWrapper.getBook().getCurrencies();
        Assert.assertNotNull(currencies);
        Iterator<CurrencyType> iterator = currencies.iterator();
        while (iterator.hasNext()) {
            CurrencyType currencyType = iterator.next();
            log.info("currencyType=" + currencyType);
        }

    }

    /**
     * Test open.
     *
     * @param folderName the folder name
     * @throws Exception the exception
     */
    private void testOpen(String folderName) throws Exception {
        final AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(folderName);
        Assert.assertNotNull(accountBookWrapper);

        AccountBook book = accountBookWrapper.getBook();
        Assert.assertNotNull(book);
        Assert.assertTrue(book.isValid());

        Account rootAccount = book.getRootAccount();
        Assert.assertNotNull(rootAccount);

        visitRootAccount(rootAccount);

        visitTransactions(book);

        try (StringWriter writer = new StringWriter()) {
            JsonUtils.writeAccountBookWrapper(accountBookWrapper, writer);
            writer.flush();
            String jsonStr = writer.toString();
            log.info("JSON str=" + jsonStr);
            
            checkJsonReader(jsonStr);

            checkJsonParser(jsonStr);
        }
    }

    private void checkJsonParser(String jsonStr) {
        JsonParser parser = new JsonParser();
        JsonElement jsonTree = parser.parse(jsonStr);
        Assert.assertNotNull(jsonTree);
        JsonObject jsonObject = jsonTree.getAsJsonObject();
        Assert.assertNotNull(jsonObject);
        
        JsonElement metaData = jsonObject.get(JsonUtils.TAG_METADATA);
        Assert.assertNotNull(metaData);
        Assert.assertTrue(metaData.isJsonObject());
        
        JsonElement syncableItems = jsonObject.get(JsonUtils.TAG_SYNCABLE_ITEMS);
        Assert.assertNotNull(syncableItems);
        Assert.assertTrue(syncableItems.isJsonArray());
        
        JsonElement localStorage = jsonObject.get(JsonUtils.TAG_LOCAL_STORAGE);
        Assert.assertNotNull(localStorage);
        Assert.assertTrue(localStorage.isJsonObject());
    }

    private void checkJsonReader(String jsonStr) throws IOException {
        JsonReader jsonReader = new JsonReader(new StringReader(jsonStr));
        while (jsonReader.hasNext()) {
            JsonToken nextToken = jsonReader.peek();
            switch (nextToken) {
            case BEGIN_ARRAY:
                jsonReader.beginArray();
                break;
            case BEGIN_OBJECT:
                jsonReader.beginObject();
                break;
            case BOOLEAN:
                boolean booleanValue = jsonReader.nextBoolean();
                break;
            case END_ARRAY:
                jsonReader.endArray();
                break;
            case END_DOCUMENT:
                break;
            case END_OBJECT:
                jsonReader.endObject();
                break;
            case NAME:
                String name = jsonReader.nextName();
                break;
            case NULL:
                break;
            case NUMBER:
                long number = jsonReader.nextLong();
                break;
            case STRING:
                String strValue = jsonReader.nextString();
                break;
            }
        }
    }

    private void visitTransactions(AccountBook book) {
        TransactionSet transactions = book.getTransactionSet();
        visitTransactions(transactions);
    }

    private void visitTransactions(TransactionSet transactions) {
        for (AbstractTxn transaction : transactions) {
            visitTransaction(transaction);
        }
    }

    private void visitTransaction(AbstractTxn transaction) {
        Account account = transaction.getAccount();
        if (TransactionUtils.isParent(transaction)) {
            if (account != null) {
                log.info("ParentTxn account=" + AccountUtils.getHierarchicalName(account) + ", uuid="
                        + account.getUUID());

                AccountType accountType = account.getAccountType();
                // isAccount
                Assert.assertFalse(AccountUtils.isRoot(accountType));
                Assert.assertTrue(AccountUtils.isAccount(accountType));
                Assert.assertFalse(AccountUtils.isCategory(accountType));
            }
        } else if (TransactionUtils.isSplit(transaction)) {
            if (account != null) {
                Account category = account;
                log.info("SplitTxn category=" + CategoryUtils.getHierarchicalName(category) + ", uuid="
                        + category.getUUID());

                AccountType categoryType = category.getAccountType();
                // isCategory
                Assert.assertFalse(AccountUtils.isRoot(categoryType));
                Assert.assertFalse(AccountUtils.isAccount(categoryType));
                Assert.assertTrue(AccountUtils.isCategory(categoryType));
            }
        } else {
            Assert.fail("Unknown transaction class=" + transaction.getClass().getName());
        }
    }

    /**
     * Visit root account.
     *
     * @param rootAccount the root account
     */
    private void visitRootAccount(Account rootAccount) {
        Assert.assertNotNull(rootAccount);

        AccountType accountType = rootAccount.getAccountType();
        // isRoot
        Assert.assertTrue(AccountUtils.isRoot(accountType));
        Assert.assertFalse(AccountUtils.isAccount(accountType));
        Assert.assertFalse(AccountUtils.isCategory(accountType));

        log.info("root=" + CategoryUtils.getHierarchicalName(rootAccount));

        for (Account subAccount : rootAccount.getSubAccounts()) {
            accountType = subAccount.getAccountType();
            if (AccountUtils.isAccount(accountType)) {
                visitAccount(subAccount);
            } else if (AccountUtils.isCategory(accountType)) {
                visitCategory(subAccount);
            } else {
                log.error("Unknown accountType=" + accountType);
            }
        }
    }

    /**
     * Visit account.
     *
     * @param account the account
     */
    private void visitAccount(Account account) {
        Assert.assertNotNull(account);
        log.info("account=" + CategoryUtils.getHierarchicalName(account));

        AccountType accountType = account.getAccountType();
        // isAccount or isCategory
        Assert.assertFalse("accountType=" + accountType, AccountUtils.isRoot(accountType));
        Assert.assertTrue("accountType=" + accountType,
                AccountUtils.isAccount(accountType) || AccountUtils.isCategory(accountType));

        for (Account subAccount : account.getSubAccounts()) {
            accountType = subAccount.getAccountType();
            if (AccountUtils.isAccount(accountType)) {
                visitAccount(subAccount);
            } else if (AccountUtils.isCategory(accountType)) {
                visitCategory(subAccount);
            } else {
                log.error("Unknown accountType=" + accountType);
            }
        }
    }

    /**
     * Visit category.
     *
     * @param category the category
     */
    private void visitCategory(Account category) {
        Assert.assertNotNull(category);
        log.info("category=" + CategoryUtils.getHierarchicalName(category));

        AccountType categoryType = category.getAccountType();

        // isCategory
        Assert.assertFalse(AccountUtils.isRoot(categoryType));
        Assert.assertFalse(AccountUtils.isAccount(categoryType));
        Assert.assertTrue(AccountUtils.isCategory(categoryType));

        Account found = CategoryUtils.getCategory(category.getParentAccount(), category.getAccountName());
        Assert.assertNotNull(found);
        Assert.assertEquals(CategoryUtils.getHierarchicalName(category), CategoryUtils.getHierarchicalName(found));

        for (Account subCategory : category.getSubAccounts()) {
            visitCategory(subCategory);
        }
    }

}
