package com.hleofxquotes.md.io;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Enumeration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.infinitekind.util.StreamTable;
import com.infinitekind.util.StreamVector;
import com.infinitekind.util.StringEncodingException;
import com.moneydance.security.KeyUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenStreamTable.
 */
public class OpenStreamTableTest {

    /** The log. */
    private Log log = LogFactory.getLog(OpenStreamTableTest.class);

    private static final String DEFAULT_HOME_ACCTS = "/localized/strings/default_home.accts";

    /** The Constant ROOT. */
    private static final String ROOT = "root";

    /** The Constant ACCT_NAME. */
    private static final String ACCT_NAME = "acct_name";

    /** The Constant ACCT_TYPE. */
    private static final String ACCT_TYPE = "acct_type";

    /** The Constant SUB_ACCOUNTS. */
    private static final String SUB_ACCOUNTS = "sub_accounts";

    /** The Constant CURRENCY_TYPE. */
    private static final String CURRENCY_TYPE = "currency_type";

    /** The Constant TAX_CAT. */
    private static final String TAX_CAT = "tax_cat";

    /**
     * Test open a stream table.
     *
     * @throws IOException             Signals that an I/O exception has occurred.
     * @throws StringEncodingException the string encoding exception
     */
    @Test
    public void testOpenStreamTable() throws IOException, StringEncodingException {
        // /localized/strings/default_home.accts
        String fileType = DEFAULT_HOME_ACCTS;

        StreamTable streamTable = new StreamTable();
        try (InputStream in = this.getClass().getResourceAsStream(fileType)) {
            assertNotNull(in);
            streamTable.readFrom(in);

//            final String accountName = ROOT;
            StreamTable rootTable = (StreamTable) streamTable.get(ROOT);
            assertNotNull(rootTable);

            StreamVector subAccounts = (StreamVector) rootTable.get(SUB_ACCOUNTS);
            visitSubAccountTable(null, subAccounts);
        }
    }

    /**
     * Visit sub accounts.
     *
     * @param parentName the parent name
     * @param vector     the account
     */
    private void visitSubAccountTable(String parentName, StreamVector vector) {
        if (vector == null) {
            return;
        }

        Enumeration<StreamTable> accounts = vector.elements();
        while (accounts.hasMoreElements()) {
            StreamTable accountTable = accounts.nextElement();
            assertNotNull(accountTable);

            String accountName = visitAccountTable(parentName, accountTable);
            StreamVector subAccountTable = (StreamVector) accountTable.get(SUB_ACCOUNTS);
            visitSubAccountTable(accountName, subAccountTable);
        }
    }

    private String visitAccountTable(String parentName, StreamTable subAccountTable) {
        String accountName = (String) subAccountTable.get(ACCT_NAME);
        assertNotNull(accountName);

        String accountType = (String) subAccountTable.get(ACCT_TYPE);
        assertNotNull(accountType);

        String currencyType = (String) subAccountTable.get(CURRENCY_TYPE);
        assertNotNull(currencyType);

        String taxCategory = (String) subAccountTable.get(TAX_CAT, (Object) null);
        // assertNotNull(acctTaxC);

        if (parentName != null) {
            log.info(parentName + ":" + accountName + ", " + accountType + ", " + currencyType + ", " + taxCategory);
        } else {
            log.info(accountName + ", " + accountType + ", " + currencyType + ", " + taxCategory);
        }
        return accountName;
    }

    @Test
    public void testKeys() throws Exception {
        // /com/moneydance/apps/md/etc/pubkeys/1.dict
        String resource = null;
        String[] resources = { "0", "1", 
//                "19",
                };
        for (String r : resources) {
            resource = "/com/moneydance/apps/md/etc/pubkeys/" + r + ".dict";
            log.info("resource=" + resource);

            try (InputStream stream = KeyUtil.class.getResourceAsStream(resource)) {
                Assert.assertNotNull(stream);
            }

            PublicKey pubKey = KeyUtil.getPublicKey(resource);
            Assert.assertNotNull(pubKey);
            String algorithm = pubKey.getAlgorithm();
            log.info("algorithm=" + algorithm);
            Signature sig = Signature.getInstance(algorithm);
            Assert.assertNotNull(sig);
            
            sig.initVerify(pubKey);
        }
    }
}
