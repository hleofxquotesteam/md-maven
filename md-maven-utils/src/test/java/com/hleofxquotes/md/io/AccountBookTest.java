package com.hleofxquotes.md.io;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.security.SecretKeyCallback;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountBookTest.
 */
public class AccountBookTest {

    private static final String DEFAULT_CURRENCY = "USD";

    /**
     * Test create account book wrapper.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCreateAccountBookWrapper() throws Exception {
        File docFolder = new File("target/docFolder");
        if (!docFolder.exists()) {
            docFolder.mkdirs();
        }
        Assert.assertTrue(docFolder.exists());
        Assert.assertTrue(docFolder.isDirectory());
        AccountBookWrapper wrapper = AccountBookWrapper.createWrapperForNewFolder(docFolder);
        Assert.assertNotNull(wrapper);

        SecretKeyCallback callback = new NullSecretKeyCallback();
        wrapper.loadLocalStorage(callback);
        String defaultCurrency = DEFAULT_CURRENCY;
        wrapper.getBook().initializeNewEmptyAccounts(defaultCurrency);
    }
}
