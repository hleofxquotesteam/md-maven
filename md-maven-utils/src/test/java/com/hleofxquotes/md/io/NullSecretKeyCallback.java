package com.hleofxquotes.md.io;

import com.moneydance.security.SecretKeyCallback;

final class NullSecretKeyCallback implements SecretKeyCallback {
    @Override
    public String getPassphrase(String var1, String var2) {
        return null;
    }

    @Override
    public String getPassphrase(String var1) {
        // TODO Auto-generated method stub
        return null;
    }
}