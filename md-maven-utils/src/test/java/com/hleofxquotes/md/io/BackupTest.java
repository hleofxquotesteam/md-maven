package com.hleofxquotes.md.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.hleofxquotes.md.BackupUtils;
import com.hleofxquotes.md.account.AccountUtils;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountIterator;
import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.security.SecretKeyCallback;

public class BackupTest {
    private static final Log LOGGER = LogFactory.getLog(BackupTest.class);

    @Test
    public void testBackupDir() throws URISyntaxException, IOException {
        URL resource = this.getClass().getResource("backups");
        Assert.assertNotNull(resource);
        URI uri = resource.toURI();
        File dir = new File(uri);
        Assert.assertTrue(dir.isDirectory());

        File expandedDirectory = null;
        int counter = 0;
        for (File file : dir.listFiles()) {
            String name = file.getName();
            if (!name.endsWith(".moneydancearchive")) {
                continue;
            }
            testOpenZipFile(file);

            if (expandedDirectory != null) {
                try {
                    BackupUtils.deleteDirectoryRecursively(expandedDirectory.toPath());
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            expandedDirectory = testOpenBackup(file);
            Assert.assertNotNull(expandedDirectory);

            counter++;
        }
        if (expandedDirectory != null) {
            try {
                BackupUtils.deleteDirectoryRecursively(expandedDirectory.toPath());
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        Assert.assertEquals(7, counter);
    }

    private File testOpenBackup(File archiveFile) throws MdFileOpenException {
        File expandedDirectory = BackupUtils.expandArchiveFile(archiveFile);

        SecretKeyCallback callback = null;
        AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(expandedDirectory.getAbsolutePath(),
                callback);
        Assert.assertNotNull(accountBookWrapper);

        AccountIterator accountIterator = new AccountIterator(accountBookWrapper.getBook());
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();
            if (AccountUtils.isRoot(account)) {
            } else if (AccountUtils.isAccount(account)) {
                String name = account.getAccountName();
                AccountType type = account.getAccountType();
                long startBalance = account.getUserStartBalance();
                long balance = account.getUserBalance();
                long currentBalance = account.getUserCurrentBalance();
                LOGGER.info("name=" + name + ", type=" + type + ", startBalance=" + startBalance + ", balance="
                        + balance + ", currentBalance=" + currentBalance);

            } else if (AccountUtils.isSecurity(account)) {
            } else if (AccountUtils.isCategory(account)) {
            } else {
            }
        }
        return expandedDirectory;
    }

    private void testOpenZipFile(File zipFile) throws IOException {
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }
}
