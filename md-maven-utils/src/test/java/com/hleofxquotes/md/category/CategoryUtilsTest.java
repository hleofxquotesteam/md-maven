package com.hleofxquotes.md.category;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;

/**
 * The Class CategoryUtilsTest.
 */
public class CategoryUtilsTest {

    /** The log. */
    private Log log = LogFactory.getLog(CategoryUtilsTest.class);

    @Mock
    AccountBook accountBook;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    /**
     * Test default categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testDefaultCategories() throws IOException {
        DefaultCategories defaultCategories = new DefaultCategories();
    }

    @Test
    @Ignore
    public void testParentIsNull() {
        Account parent = null;
        String name = null;
        Account category = CategoryUtils.getCategory(parent, name);
        Assert.assertNull(category);
    }

    @Test
    @Ignore
    public void testNameIsNull() {
        Account parent = new Account(accountBook);
        parent.setAccountType(AccountType.ROOT);

        String name = null;
        Account category = CategoryUtils.getCategory(parent, name);
        Assert.assertNull(category);
    }

    @Test
    @Ignore
    public void testRootName() {
        CurrencyType currencyType = new CurrencyType(new CurrencyTable(accountBook));

        Account root = new Account(accountBook);
        root.setParentAccount(null);
        root.setAccountType(AccountType.ROOT);
        root.setAccountName(AccountType.ROOT.toString());
        root.setCurrencyType(currencyType);

        Account income = new Account(accountBook);
        income.setParentAccount(root);
        root.getSubAccounts().add(income);
        income.setAccountType(AccountType.INCOME);
        income.setAccountName(AccountType.INCOME.toString());
        income.setCurrencyType(currencyType);

        Account expense = new Account(accountBook);
        expense.setParentAccount(root);
        root.getSubAccounts().add(expense);
        expense.setAccountType(AccountType.EXPENSE);
        expense.setAccountName(AccountType.EXPENSE.toString());
        expense.setCurrencyType(currencyType);

        String name = AccountType.EXPENSE.toString() + CategoryUtils.SEPARATOR + "Bills";
        log.info("name=" + name);
        Account category = CategoryUtils.getCategory(root, name);
        log.info("category.name=" + category.getAccountName());
        log.info("parentCategory=" + category.getParentAccount());
        Assert.assertEquals(name, CategoryUtils.getHierarchicalName(category));
//        Assert.assertEquals("EXPENSE", CategoryUtils.getHierarchicalName(category, true, false));
//        Assert.assertEquals("EXPENSE[EXPENSE]", CategoryUtils.getHierarchicalName(category, true, true));
//        Assert.assertEquals("EXPENSE[EXPENSE]", CategoryUtils.getHierarchicalName(category, false, true));
//        Assert.assertEquals("EXPENSE", CategoryUtils.getHierarchicalName(category, false, false));
    }
}
