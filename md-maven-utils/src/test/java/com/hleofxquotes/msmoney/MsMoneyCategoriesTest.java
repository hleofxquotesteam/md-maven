package com.hleofxquotes.msmoney;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.hleofxquotes.md.category.CategoryUtils;
import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.hleofxquotes.md.io.AccountBookWrapperUtilsTest;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencyType;
import com.infinitekind.moneydance.model.Legacy;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class MsMoneyCategoriesTest {
    private Log log = LogFactory.getLog(MsMoneyCategoriesTest.class);

    @Test
    public void testRead() throws IOException {
        MsMoneyCategories categories = getCategories();

        for (MsMoneyCategory category : categories.getCategories()) {
            boolean expense = categories.isExpense(category);
            boolean income = categories.isIncome(category);
            Assert.assertTrue(income != expense);
            log.info("category=" + category.getName() + ", expense=" + expense + ", income=" + income + ", fullName="
                    + categories.getFullName(category));
        }
    }

    @Test
    public void testAddCategories() throws IOException {
        File folder = new File(AccountBookWrapperUtilsTest.TARGET_TEST_MONEYDANCE);
        AccountBookWrapperUtils.deleteFolder(folder);
    
        AccountBookWrapper bookWrapper = AccountBookWrapperUtils.createWithBasicAccounts(folder);
        AccountBook book = bookWrapper.getBook();
        Assert.assertTrue(book.isValid());
    
        int foundCount = 0;
        int notFoundCount = 0;
    
        try {
            MsMoneyCategories msMoneyCategories = getCategories();
    
            for (MsMoneyCategory msMoneyCategory : msMoneyCategories.getCategories()) {
                boolean expense = msMoneyCategories.isExpense(msMoneyCategory);
                boolean income = msMoneyCategories.isIncome(msMoneyCategory);
                String fullName = msMoneyCategories.getFullName(msMoneyCategory);
    
                log.info("name=" + msMoneyCategory.getName() + ", fullName=" + fullName);
                fullName = fullName.trim();
                if (fullName.length() <= 0) {
                    continue;
                }
    
                Account found = null;
                if (expense) {
                    found = CategoryUtils.findExpenseCategory(bookWrapper, fullName);
                } else if (income) {
                    found = CategoryUtils.findIncomeCategory(bookWrapper, fullName);
                } else {
                    log.error("msmoney category=" + fullName + "is neither INCOME nor EXPENSE.");
                    continue;
                }
    
                if (found != null) {
                    log.warn("Found existing category=" + fullName + ", type=" + found.getAccountType());
                    foundCount++;
                } else {
                    notFoundCount++;
    
                    createNewCategories(book, fullName, expense, msMoneyCategory);
                }
            }
    
            log.info("foundCount=" + foundCount + ", notFoundCount=" + notFoundCount);
        } finally {
            book.save();
        }
    
        Assert.assertEquals(3, foundCount);
        Assert.assertEquals(136, notFoundCount);
    }

    private MsMoneyCategories getCategories() throws FileNotFoundException {
        String fileName = "src/test/resources/msmoney/exportCsv/categories.csv";
        Reader reader = new BufferedReader(new FileReader(new File(fileName)));
        MsMoneyCategories categories = new MsMoneyCategories(reader);
        return categories;
    }

    private void createNewCategories(final AccountBook book, String fullName, boolean expense,
            MsMoneyCategory category) {
        Account root = book.getRootAccount();
        Account parentAccount = root;

        String[] tokens = fullName.split(":");
        for (String token : tokens) {
            token = token.trim();
            String categoryName = token;

            log.info("fullName=" + fullName + ", accountName=" + categoryName);

            Account found = parentAccount.getAccountByName(categoryName);
            if (found == null) {
                Account newCategory = createNewCategory(book, categoryName, parentAccount, expense);
                newCategory.setComment("Imported from MsMoney, id=" + category.getId());

                parentAccount.getSubAccounts().add(newCategory);
                parentAccount.syncItem();
                newCategory.syncItem();
                found = newCategory;
            }

            parentAccount = found;
        }
    }

    private Account createNewCategory(final AccountBook book, String categoryName, Account parentAccount,
            boolean expense) {
        int accountID = -1;
        CurrencyType currency = parentAccount.getCurrencyType();
        Hashtable accountInfo = new Hashtable<>();
        Vector subAccounts = new Vector<>();

        Account newCategory;
        if (expense) {
            newCategory = Legacy.makeExpenseAccount(book, categoryName, accountID, currency, accountInfo, subAccounts,
                    parentAccount);
        } else {
            newCategory = Legacy.makeIncomeAccount(book, categoryName, accountID, currency, accountInfo, subAccounts,
                    parentAccount);
        }
        return newCategory;
    }
}
