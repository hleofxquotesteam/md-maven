package com.hleofxquotes.msmoney;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.hleofxquotes.md.io.AccountBookWrapperUtilsTest;
import com.infinitekind.moneydance.model.AccountBook;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class MsMoneyTransactionsTest {
    private Log log = LogFactory.getLog(MsMoneyTransactionsTest.class);

    @Test
    public void testRead() throws FileNotFoundException, IOException {
        MsMoneyTransactions msMoneyTransactions = getTransactions();

        for (MsMoneyTransaction transaction : msMoneyTransactions.getTransactions()) {
            Assert.assertNotNull(transaction.getDate());
        }
    }

    @Test
    public void testAddTransactions() throws IOException {
        File folder = new File(AccountBookWrapperUtilsTest.TARGET_TEST_MONEYDANCE);
        AccountBookWrapperUtils.deleteFolder(folder);

        AccountBookWrapper bookWrapper = AccountBookWrapperUtils.createWithBasicAccounts(folder);
        AccountBook book = bookWrapper.getBook();
        Assert.assertTrue(book.isValid());

        try {
            MsMoneyTransactions msMoneyTransactions = getTransactions();

            for (MsMoneyTransaction msMoneyTransaction : msMoneyTransactions.getTransactions()) {
                msMoneyTransaction.getFiTid();
            }
        } finally {
            book.save();
        }
    }

    private MsMoneyTransactions getTransactions() throws IOException, FileNotFoundException {
        MsMoneyTransactions msMoneyTransactions = null;
        String fileName = "src/test/resources/msmoney/exportCsv/transactions.csv";
        try (Reader reader = new BufferedReader(new FileReader(new File(fileName)))) {
            msMoneyTransactions = new MsMoneyTransactions(reader);
        }
        return msMoneyTransactions;
    }
}
