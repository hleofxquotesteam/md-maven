package com.hleofxquotes.msmoney;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opencsv.bean.CsvToBeanBuilder;

public class MsMoneyCategories {

    private final List<MsMoneyCategory> categories;
    private final Map<Long, MsMoneyCategory> map;

    public MsMoneyCategories(Reader reader) {
        super();

        categories = new CsvToBeanBuilder<MsMoneyCategory>(reader).withType(MsMoneyCategory.class).build().parse();
        map = new HashMap<>();

        for (MsMoneyCategory bean : categories) {
            map.put(bean.getId(), bean);
        }
    }

    public List<MsMoneyCategory> getCategories() {
        return categories;
    }

    public boolean isExpense(MsMoneyCategory category) {
        return MsMoneyCategoryUtils.isExpense(category, map);
    }

    public boolean isIncome(MsMoneyCategory category) {
        return MsMoneyCategoryUtils.isIncome(category, map);
    }

    public String getFullName(MsMoneyCategory category) {
        return MsMoneyCategoryUtils.getFullName(category, map);
    }

}
