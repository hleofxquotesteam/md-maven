package com.hleofxquotes.msmoney;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.opencsv.bean.CsvCustomBindByName;

public class MsMoneyTransaction {
// "id","date","number","amount","categoryId","category","memo","payeeId","payee","splitsCount","splits","splitParentId","accountId","account","void","cleared","reconciled","unaccepted","transfer","xferAccountId","xferTransactionId","investment","invActivity","invSecId","invSecName","invSecSymbol","invTxPrice","invTxQuantity","recurring","frequency","classification1Id","classification1Name","fiTid"
    private Long id;

    @CsvCustomBindByName(converter = LocalDateConverter.class)
    private LocalDate date;

    private String number;

    private BigDecimal amount;

    private Long categoryId;

    private String category;

    private String memo;

    private Long payeeId;

    private String payee;

    private Long splitCount;

    private String splits;

    private Long splitParentId;

    private Long accountId;

    private String account;

    private String fiTid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(Long payeeId) {
        this.payeeId = payeeId;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public Long getSplitCount() {
        return splitCount;
    }

    public void setSplitCount(Long splitCount) {
        this.splitCount = splitCount;
    }

    public String getSplits() {
        return splits;
    }

    public void setSplits(String splits) {
        this.splits = splits;
    }

    public Long getSplitParentId() {
        return splitParentId;
    }

    public void setSplitParentId(Long splitParentId) {
        this.splitParentId = splitParentId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getFiTid() {
        return fiTid;
    }

    public void setFiTid(String fiTid) {
        this.fiTid = fiTid;
    }

}
