package com.hleofxquotes.msmoney;

import java.util.Map;
import java.util.Stack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MsMoneyCategoryUtils {
    private static Log log = LogFactory.getLog(MsMoneyCategoryUtils.class);

    private static final String SEPARATOR = ":";

    private MsMoneyCategoryUtils() {
        super();
    }

    public static boolean isExpense(MsMoneyCategory category, Map<Long, MsMoneyCategory> map) {
        category = getRoot(category, map);

        if (category == null) {
            return false;
        }

        return category.getName().compareTo("EXPENSE") == 0;
    }

    public static boolean isIncome(MsMoneyCategory category, Map<Long, MsMoneyCategory> map) {
        category = getRoot(category, map);

        if (category == null) {
            return false;
        }

        return category.getName().compareTo("INCOME") == 0;
    }

    private static MsMoneyCategory getRoot(MsMoneyCategory category, Map<Long, MsMoneyCategory> map) {
        Long pid;
        // walk back up until root
        while ((pid = category.getParentId()) != null) {
            category = map.get(pid);
            if (category == null) {
                log.error("Cannot find category.id=" + pid);
                break;
            }
        }
        return category;
    }

    public static String getFullName(MsMoneyCategory category, Map<Long, MsMoneyCategory> map) {
        Stack<String> names = new Stack<String>();
        Long pid;
        // walk back up until root
        while ((pid = category.getParentId()) != null) {
            names.push(category.getName());

            category = map.get(pid);
            if (category == null) {
                log.error("Cannot find category.id=" + pid);
                break;
            }

        }

        int count = 0;
        StringBuilder sb = new StringBuilder();
        while (!names.empty()) {
            String name = names.pop();
            if (count > 0) {
                sb.append(SEPARATOR);
            }
            sb.append(name);
            count++;
        }

        return sb.toString();
    }

}
