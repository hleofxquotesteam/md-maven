package com.hleofxquotes.msmoney;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;

import com.opencsv.bean.CsvToBeanBuilder;

public class MsMoneyTransactions {
    private final List<MsMoneyTransaction> transactions;
    private final HashMap<Long, MsMoneyTransaction> map;

    public MsMoneyTransactions(Reader reader) {
        super();

        transactions = new CsvToBeanBuilder<MsMoneyTransaction>(reader).withType(MsMoneyTransaction.class).build()
                .parse();
        map = new HashMap<>();

        for (MsMoneyTransaction bean : transactions) {
            map.put(bean.getId(), bean);
        }
    }

    public List<MsMoneyTransaction> getTransactions() {
        return transactions;
    }

    public HashMap<Long, MsMoneyTransaction> getMap() {
        return map;
    }
}
