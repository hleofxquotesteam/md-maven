package com.hleofxquotes.md.transaction;

import com.infinitekind.moneydance.model.Account;

public class TransactionSplitInfo {
    private String payeeName;
    private Long amount;
    private Account category;

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Account getCategory() {
        return category;
    }

    public void setCategory(Account category) {
        this.category = category;
    }
}