package com.hleofxquotes.md.transaction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.category.CategoryUtils;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountUtil;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.util.DateUtil;

public class TransactionUtils {
    private static final Log log = LogFactory.getLog(TransactionUtils.class);

    private TransactionUtils() {
        super();
    }

    public static final ParentTxn addTransaction(AccountBook book, Account account, List<TransactionSplitInfo> splits,
            String keyword) {

        if ((splits == null) || (splits.size() <= 0)) {
            log.warn("Cannot add transctions with empty splits");
            return null;
        }

        int date = DateUtil.getStrippedDateInt();
        int taxDate = date;
        long dateEntered = System.currentTimeMillis();

        String checkNumber = "";

        TransactionSplitInfo rootSplit = splits.get(0);

        String description = rootSplit.getPayeeName();

        String memo = "memo - Created for testing";

        long id = -1L;

        byte status = ParentTxn.STATUS_UNRECONCILED;

        ParentTxn parentTxn = null;
        try {
            parentTxn = ParentTxn.makeParentTxn(book, date, taxDate, dateEntered, checkNumber, account, description,
                    memo, id, status);

            if (!StringUtils.isBlank(keyword)) {
                List<String> keywords = null;
                keywords = parentTxn.getKeywords();
                if ((keywords == null) || (keywords.size() <= 0)) {
                    keywords = new ArrayList<>();
                }
                keywords.add(keyword);
                parentTxn.setKeywords(keywords);
            }

            parentTxn.setIsNew(true);

            addSplits(book, account, parentTxn, splits, description, status, keyword);
        } finally {
            if (parentTxn != null) {
                book.getTransactionSet().addNewTxn(parentTxn);
                parentTxn.syncItem();
//        book.refreshAccountBalances();
            }
        }

        return parentTxn;
    }

    private static void addSplits(AccountBook book, Account account, ParentTxn parentTxn,
            List<TransactionSplitInfo> splits, String description, byte status, String keyword) {

        for (TransactionSplitInfo split : splits) {
            long splitAmount = split.getAmount();
            if (!account.balanceIsNegated()) {
                splitAmount = -splitAmount;
            }
            long parentAmount = splitAmount;

            double rate = 1.0;

            Account category = split.getCategory();

            long txnId = -1L;

            SplitTxn splitTxn = SplitTxn.makeSplitTxn(parentTxn, parentAmount, splitAmount, rate, category, description,
                    txnId, status);

            if (!StringUtils.isBlank(keyword)) {
                List<String> keywords;
                keywords = splitTxn.getKeywords();
                if ((keywords == null) || (keywords.size() <= 0)) {
                    keywords = new ArrayList<>();
                }
                keywords.add(keyword);
                splitTxn.setKeywords(keywords);
            }

            splitTxn.setIsNew(true);
            splitTxn.syncItem();

            parentTxn.addSplit(splitTxn);
        }
    }

    public static Account getCategory(AccountBook book, Account account, String categoryName) {
//        String categoryName = split.getCategoryName();
        Account category = CategoryUtils.getCategory(book.getRootAccount(), categoryName);
        if (category == null) {
            category = AccountUtil.getDefaultCategoryForAcct(account);
        }
        log.info("NEW transaction.category=" + category);
        return category;
    }

    public static final void addTransaction(AccountBook book, Account account, long amount, Account category,
            String payeeName) {
        List<TransactionSplitInfo> splits = new ArrayList<>();
        TransactionSplitInfo split = new TransactionSplitInfo();
        split.setAmount(amount);
        split.setCategory(category);
        split.setPayeeName(payeeName);
        splits.add(split);
        addTransaction(book, account, splits);
    }

    public static ParentTxn addTransaction(AccountBook book, Account account, List<TransactionSplitInfo> splits) {
        String keyword = null;
        return addTransaction(book, account, splits, keyword);
    }

    public static final boolean isParent(AbstractTxn transaction) {
        return transaction instanceof ParentTxn;
    }

    public static final boolean isSplit(AbstractTxn transaction) {
        return transaction instanceof SplitTxn;
    }

    public static final Stream<AbstractTxn> getTransactionStream(AccountBook book) {
        Iterator<AbstractTxn> iterator = book.getTransactionSet().iterator();
        Spliterator<AbstractTxn> spliterator = Spliterators.spliteratorUnknownSize(iterator, 0);
        return StreamSupport.stream(spliterator, false);
    }
}
