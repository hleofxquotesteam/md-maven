package com.hleofxquotes.md.category;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultCategories.
 */
public class DefaultCategories extends AbstractCategories {

    /** The log. */
    private Log log = LogFactory.getLog(DefaultCategories.class);

    /**
     * Instantiates a new default categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public DefaultCategories() throws IOException {
        super();
        init();
    }

    /**
     * Inits the.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void init() throws IOException {
        String resourceName = DEFAULT_CATEGORIES;

        load(resourceName);
    }

    /**
     * Visit.
     *
     * @param parentName   the parent name
     * @param name         the name
     * @param type         the type
     * @param currencyType the currency type
     * @param taxCategory  the tax category
     */
    protected void visit(String parentName, String name, String type, String currencyType, String taxCategory) {
        if (parentName != null) {
            if (log.isDebugEnabled()) {
                log.debug(parentName + ":" + name + ", " + type + ", " + currencyType + ", " + taxCategory);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug(name + ", " + type + ", " + currencyType + ", " + taxCategory);
            }
        }
    }

}
