package com.hleofxquotes.md.category;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.infinitekind.util.StreamTable;
import com.infinitekind.util.StreamVector;
import com.infinitekind.util.StringEncodingException;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractCategories.
 */
public abstract class AbstractCategories {

    private static final String COL_TAX_CAT = "tax_cat";

    private static final String COL_CURRENCY_TYPE = "currency_type";

    private static final String COL_ACCT_TYPE = "acct_type";

    private static final String COL_ACCT_NAME = "acct_name";

    private static final String SUB_ACCOUNTS_TABLE_NAME = "sub_accounts";

    private static final String ROOT_TABLE_NAME = "root";

    /** The log. */
    private Log log = LogFactory.getLog(AbstractCategories.class);

    /** The Constant DEFAULT_CATEGORIES. */
    public static final String DEFAULT_CATEGORIES = "/localized/strings/default_home.accts";

    /**
     * Load default categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void loadDefaultCategories() throws IOException {
        load(AbstractCategories.DEFAULT_CATEGORIES);
    }

    /**
     * Load.
     *
     * @param resourceName the resource name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void load(String resourceName) throws IOException {
        try (InputStream in = this.getClass().getResourceAsStream(resourceName)) {
            StreamTable streamTable = new StreamTable();
            streamTable.readFrom(in);

            StreamTable rootTable = getRootTable(streamTable);

            StreamVector subCategories = getSubCategories(rootTable);

            String parentName = null;
            visitSubCategories(parentName, subCategories);
        } catch (StringEncodingException e) {
            throw new IOException(e);
        }
    }

    /**
     * Gets the root table.
     *
     * @param streamTable the stream table
     * @return the root table
     */
    private StreamTable getRootTable(StreamTable streamTable) {
        final String name = ROOT_TABLE_NAME;
        final StreamTable rootTable = (StreamTable) streamTable.get(name);
        return rootTable;
    }

    /**
     * Gets the sub categories.
     *
     * @param streamTable the stream table
     * @return the sub categories
     */
    private StreamVector getSubCategories(StreamTable streamTable) {
        StreamVector subCategories = (StreamVector) streamTable.get(SUB_ACCOUNTS_TABLE_NAME);
        return subCategories;
    }

    /**
     * Visit sub categories.
     *
     * @param parentName   the parent name
     * @param streamVector the account
     */
    private void visitSubCategories(String parentName, StreamVector streamVector) {
        if (streamVector == null) {
            return;
        }

        Enumeration<StreamTable> elements = streamVector.elements();
        while (elements.hasMoreElements()) {
            StreamTable accountTable = elements.nextElement();

            String name = (String) accountTable.get(COL_ACCT_NAME);

            String type = (String) accountTable.get(COL_ACCT_TYPE);

            String currencyType = (String) accountTable.get(COL_CURRENCY_TYPE);

            String taxCategory = (String) accountTable.get(COL_TAX_CAT, (Object) null);

            visit(parentName, name, type, currencyType, taxCategory);

//            StreamVector subAccounts = (StreamVector) accountTable.get("sub_accounts");
            StreamVector subCategories = getSubCategories(accountTable);

            visitSubCategories(name, subCategories);
        }
    }

    /**
     * Visit.
     *
     * @param parentName   the parent name
     * @param name         the name
     * @param type         the type
     * @param currencyType the currency type
     * @param taxCategory  the tax category
     */
    protected abstract void visit(String parentName, String name, String type, String currencyType, String taxCategory);
}
