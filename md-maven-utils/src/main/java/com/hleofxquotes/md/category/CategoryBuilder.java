package com.hleofxquotes.md.category;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountBook;

public class CategoryBuilder {

    private AccountBook book;
    private AccountType accountType;
    private String accountName;
    private Account parentAccount;

    public CategoryBuilder() {
        super();
    }

    public Category build() {
        Category category = new Category(book);
        category.setAccountType(accountType);
        category.setAccountName(accountName);
        category.setParentAccount(parentAccount);

        category.ensureAccountStructure();
        category.syncItem();

        return category;
    }

    public AccountBook getBook() {
        return book;
    }

    public void setBook(AccountBook book) {
        this.book = book;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Account getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(Account parentAccount) {
        this.parentAccount = parentAccount;
    }
}
