package com.hleofxquotes.md.category;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.account.AccountUtils;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountIterator;
import com.moneydance.apps.md.controller.AccountBookWrapper;

// TODO: Auto-generated Javadoc
/**
 * The Class CategoryUtils.
 */
public class CategoryUtils {

    /** The Constant log. */
    private static final Log log = LogFactory.getLog(CategoryUtils.class);

    /** The Constant SEPARATOR_CHAR. */
    private static final char SEPARATOR_CHAR = ':';

    /** The Constant SEPARATOR. */
    static final String SEPARATOR = "" + SEPARATOR_CHAR;

    /**
     * Gets the category.
     * 
     * @param parent the parent
     * @param name   the name
     *
     * @return the category
     */
    public static final Account getCategory(Account parent, String name, boolean create) {
        if (parent == null) {
            return null;
        }

        if (name == null) {
            return null;
        }

        if (name.startsWith(SEPARATOR) && parent.getAccountType() == Account.AccountType.ROOT) {
            name = name.substring(1);
        }

        String restOfAcctName;
        String thisAcctName;
        int colIndex = name.indexOf(SEPARATOR_CHAR);
        if (colIndex >= 0) {
            restOfAcctName = name.substring(colIndex + 1);
            thisAcctName = name.substring(0, colIndex);
        } else {
            restOfAcctName = null;
            thisAcctName = name;
        }

        // find an existing category
        for (int i = 0; i < parent.getSubAccountCount(); i++) {
            Account subCategory = parent.getSubAccount(i);
            Account.AccountType subCategoryType = subCategory.getAccountType();
            if (!(subCategoryType == Account.AccountType.BANK || subCategoryType == Account.AccountType.CREDIT_CARD
                    || subCategoryType == Account.AccountType.EXPENSE
                    || subCategoryType == Account.AccountType.INCOME)) {
                continue;
            }
            if (subCategory.getAccountName().equalsIgnoreCase(thisAcctName)) {
                if (restOfAcctName == null) {
                    return subCategory;
                } else {
                    return getCategory(subCategory, restOfAcctName);
                }
            }
        }

        // not found
        if (create) {
            Account newCategory = AccountUtils.createAccount(parent, thisAcctName);
            if (restOfAcctName == null) {
                return newCategory;
            } else {
                return getCategory(newCategory, restOfAcctName);
            }
        } else {
            return null;
        }
    }

    public static Account getCategory(Account parent, String name) {
        boolean create = true;
        return getCategory(parent, name, create);
    }

    /**
     * Gets the hierarchical name.
     *
     * @param category  the account
     * @param childName the child name
     * @param showRoot  the show root
     * @param showType  the show account type
     * @return the hierarchical name
     */
    private static String getHierarchicalName(Account category, String childName, boolean showRoot, boolean showType) {
        String newName = childName;

        if (log.isDebugEnabled()) {
            log.debug("> getHierarchicalName: name=" + category.getAccountName() + ", childName=" + childName
                    + ", showRoot=" + showRoot + ", showtType=" + showType);
        }

        String name = category.getAccountName();
        AccountType type = category.getAccountType();
        if (showType) {
            name = name + "[" + type + "]";
        }

        if (AccountUtils.isRoot(type)) {
            if (!showRoot) {
                name = "";
            }
        }

        if ((childName != null) && (childName.length() > 0)) {
            childName = name + SEPARATOR + childName;
        } else {
            childName = name;
        }

        // go up to parent
        Account parentCategory = category.getParentAccount();
        if (parentCategory != null) {
            newName = getHierarchicalName(parentCategory, childName, showRoot, showType);
        }

        return newName;
    }

    /**
     * Gets the hierarchical name.
     *
     * @param category the account
     * @return the hierarchical name
     */
    public static String getHierarchicalName(Account category) {
        boolean showRoot = false;
        boolean showType = false;
        String childName = "";

        // if the account is root, take care of it immediately.
        AccountType type = category.getAccountType();
        if (AccountUtils.isRoot(type)) {
            if (!showRoot) {
                String name = category.getAccountName();
                if (showType) {
                    name = name + "[" + type + "]";
                }
                return name;
            }
        }

        return CategoryUtils.getHierarchicalName(category, childName, showRoot, showType);
    }

    public static String getHierarchicalName(Account category, boolean showRoot, boolean showType) {
        String childName = "";

        // if the account is root, take care of it immediately.
        AccountType type = category.getAccountType();
        if (AccountUtils.isRoot(type)) {
            if (!showRoot) {
                String name = category.getAccountName();
                if (showType) {
                    name = name + "[" + type + "]";
                }
                return name;
            }
        }

        return CategoryUtils.getHierarchicalName(category, childName, showRoot, showType);
    }

    public static Account findCategory(AccountBookWrapper bookWrapper, String name) {
        AccountIterator accountIterator = new AccountIterator(bookWrapper.getBook());
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();
            if (AccountUtils.isRoot(account)) {
            } else if (AccountUtils.isAccount(account)) {
            } else if (AccountUtils.isSecurity(account)) {
            } else if (AccountUtils.isCategory(account)) {
                if (account.toString().compareToIgnoreCase(name) == 0) {
                    return account;
                }
            } else {
            }
        }

        return null;
    }

    public static List<Account> getCategories(AccountBookWrapper bookWrapper) {
        List<Account> roots = new ArrayList<>();
        List<Account> accounts = new ArrayList<>();
        List<Account> securities = new ArrayList<>();
        List<Account> categories = new ArrayList<>();
        List<Account> unknowns = new ArrayList<>();

        AccountIterator accountIterator = new AccountIterator(bookWrapper.getBook());
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();
            if (AccountUtils.isRoot(account)) {
                roots.add(account);
            } else if (AccountUtils.isAccount(account)) {
                accounts.add(account);
            } else if (AccountUtils.isSecurity(account)) {
                securities.add(account);
            } else if (AccountUtils.isCategory(account)) {
                categories.add(account);
            } else {
                unknowns.add(account);
            }
        }

        return categories;
    }

    public static Account findExpenseCategory(AccountBookWrapper bookWrapper, String name) {
        Account account = CategoryUtils.findCategory(bookWrapper, name);
        if (account != null) {
            AccountType accountType = account.getAccountType();
            if (accountType != AccountType.EXPENSE) {
                account = null;
            }
        }
        return account;
    }

    public static Account findIncomeCategory(AccountBookWrapper bookWrapper, String name) {
        Account account = CategoryUtils.findCategory(bookWrapper, name);
        if (account != null) {
            AccountType accountType = account.getAccountType();
            if (accountType != AccountType.INCOME) {
                account = null;
            }
        }
        return account;
    }

}
