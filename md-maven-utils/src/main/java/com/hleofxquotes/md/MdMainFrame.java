package com.hleofxquotes.md;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.account.AccountUtils;
import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.tiksync.SyncableItem;
import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.apps.md.controller.Main;
import com.moneydance.apps.md.controller.olb.MoneybotURLStreamHandlerFactory;
import com.moneydance.security.SecretKeyCallback;

public class MdMainFrame extends JFrame {
    private static final Log LOGGER = LogFactory.getLog(MdMainFrame.class);

    private static final String SUFFIX_MONEYDANCEARCHIVE = ".moneydancearchive";

    static final String SUFFIX_MONEYDANCE = ".moneydance";

    private MyLoginDialog loginDialog;
    private String password;
    private File moneydanceFile;

    private ExecutorService threadPool = Executors.newCachedThreadPool();

    protected AccountBookWrapper accountBookWrapper;

    private AbstractTableModel model;

    private File expandedDirectory;

    public MdMainFrame(String title) {
        super(title);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initView(getContentPane());

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (expandedDirectory != null) {
                try {
                    BackupUtils.deleteDirectoryRecursively(expandedDirectory.toPath());
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }));
    }

    private void initView(Container contentPane) {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());
        view.setPreferredSize(new Dimension(600, 400));

        addDropHandler(view);

        model = new AbstractTableModel() {

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Object value = null;
                if (accountBookWrapper == null) {
                    return null;
                }

                List<SyncableItem> items = accountBookWrapper.getBook().getSyncer().getSyncedDocument().allItems();

                SyncableItem item = items.get(rowIndex);

                switch (columnIndex) {
                case 0:
                    value = item.getUUID();
                    break;
                case 1:
                    value = item.getSyncItemType();
                    break;
                default:
                    value = null;
                    break;
                }

                return value;
            }

            @Override
            public int getRowCount() {
                if (accountBookWrapper == null) {
                    return 0;
                }

                List<SyncableItem> items = accountBookWrapper.getBook().getSyncer().getSyncedDocument().allItems();

                return items.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }
        };
        JTable table = new JTable(model);

        JScrollPane sp = new JScrollPane(table);
        view.add(sp, BorderLayout.CENTER);

        contentPane.add(view);
    }

    private void addDropHandler(JPanel view) {
        TransferHandler handler = new FileDropHandler() {

            @Override
            public void handleFile(File file) {
                LOGGER.info("DROP file=" + file.getAbsolutePath());
                if (file.isDirectory()) {
                    String name = file.getName();
                    if (name.endsWith(SUFFIX_MONEYDANCE)) {
                        handleMoneydanceFile(file);
                    } else {
                        LOGGER.warn("SKIP file=" + file.getAbsolutePath() + " is NOT a moneydance file.");
                    }
                } else if (file.isFile()) {
                    String name = file.getName();
                    if (name.endsWith(SUFFIX_MONEYDANCEARCHIVE)) {
                        if (expandedDirectory != null) {
                            try {
                                BackupUtils.deleteDirectoryRecursively(expandedDirectory.toPath());
                            } catch (IOException e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                        }
                        expandedDirectory = handleMoneydanceArchiveFile(file);
                        if (expandedDirectory == null) {
                            LOGGER.error("Cannot unzip archive file=" + file.getAbsolutePath());
                        } else {
                            handleMoneydanceFile(expandedDirectory);
                        }
                    } else {
                        LOGGER.warn("SKIP file=" + file.getAbsolutePath() + " is NOT a moneydancearchive file.");
                    }
                } else {
                    LOGGER.warn("SKIP file=" + file.getAbsolutePath());
                }
            }
        };
        view.setTransferHandler(handler);

        ActionListener loginAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                password = loginDialog.getPassword();
                loginDialog.dispose();

                openMoneydanceFile(moneydanceFile, password);
            }
        };
        loginDialog = new MyLoginDialog(MdMainFrame.this, loginAction, false);
        showLoginDialog(false);
    }

    protected void openMoneydanceFile(File moneydanceFile, final String password) {
        Runnable command = new Runnable() {

            @Override
            public void run() {
                SecretKeyCallback callback = null;
                if (StringUtils.isNotEmpty(password)) {
                    callback = new SecretKeyCallback() {

                        @Override
                        public String getPassphrase(String arg0, String arg1) {
                            return password;
                        }

                        @Override
                        public String getPassphrase(String arg0) {
                            return password;
                        }
                    };
                }
                try {
                    accountBookWrapper = AccountBookWrapperUtils.open(moneydanceFile.getAbsolutePath(), callback);
                    notifyAccountBookWrapperOpened(accountBookWrapper);
                } catch (Exception e) {
                    notifyAccountBookWrapperException(e);
                } finally {
                }

            }
        };
        threadPool.execute(command);

    }

    protected void notifyAccountBookWrapperException(Exception e) {
        LOGGER.error(e.getMessage(), e);
    }

    protected void notifyAccountBookWrapperOpened(final AccountBookWrapper accountBookWrapper) {
        LOGGER.info("> notifyAccountBookWrapperOpened, accountBookWrapper=" + accountBookWrapper);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if (model != null) {
                    model.fireTableDataChanged();
                }

                AccountIterator accountIterator = new AccountIterator(accountBookWrapper.getBook());
                while (accountIterator.hasNext()) {
                    Account account = accountIterator.next();
                    if (AccountUtils.isRoot(account)) {
                    } else if (AccountUtils.isAccount(account)) {
                        String name = account.getAccountName();
                        AccountType type = account.getAccountType();
                        long startBalance = account.getUserStartBalance();
                        long balance = account.getUserBalance();
                        long currentBalance = account.getUserCurrentBalance();
                        LOGGER.info("name=" + name + ", type=" + type + ", startBalance=" + startBalance + ", balance="
                                + balance + ", currentBalance=" + currentBalance);

                    } else if (AccountUtils.isSecurity(account)) {
                    } else if (AccountUtils.isCategory(account)) {
                    } else {
                    }
                }
            }
        });

    }

    private void showLoginDialog(boolean visible) {
        LOGGER.info("> showLoginDialog, visible=" + visible);

        loginDialog.setLocationRelativeTo(MdMainFrame.this);
        loginDialog.pack();
        loginDialog.setVisible(visible);
    }

    protected File handleMoneydanceArchiveFile(File archiveFile) {
        return BackupUtils.expandArchiveFile(archiveFile);
    }

    protected void handleMoneydanceFile(File file) {
        this.moneydanceFile = file;
        showLoginDialog(true);
    }

    public static void main(String[] args) {
        // Make it portable by setting home directory to current directory
        File cwd = new File(".");
        File homeDir = new File(cwd, "moneyDance");
        homeDir.mkdirs();

        String userHomeDirectory = homeDir.getAbsolutePath();
        System.setProperty("user.home", userHomeDirectory);
        System.out.println("user.home=" + System.getProperty("user.home"));

        Main.DEBUG = true;
        MoneybotURLStreamHandlerFactory.DEBUG = true;

        ArrayList<String> argList = new ArrayList<String>(Arrays.asList(args));
        argList.add("-d");
        args = argList.toArray(args);

//        DEBUG = true;
        System.setProperty("moneydance.debug", "true");

        String title = MdMainFrame.class.getName();

        final MdMainFrame mainFrame = new MdMainFrame(title);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                mainFrame.setLocation(100, 100);
                mainFrame.pack();
                mainFrame.setVisible(true);
            }
        });
    }
}
