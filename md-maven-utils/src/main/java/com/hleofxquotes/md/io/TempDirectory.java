package com.hleofxquotes.md.io;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class TempDirectory.
 */
public class TempDirectory {

    /** The Constant log. */
    private static final Log log = LogFactory.getLog(TempDirectory.class);

    /** The path. */
    final Path path;

    /**
     * Instantiates a new temp directory.
     *
     * @param prefix the prefix
     */
    public TempDirectory(String prefix) {
        try {
            path = Files.createTempDirectory(prefix);
            log.info(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public Path getPath() {
        return path;
    }

    /**
     * Delete on exit.
     */
    public final void deleteOnExit() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                delete(getPath());
            }
        });
    }

    /**
     * Delete.
     *
     * @param path the path
     */
    public static final void delete(Path path) {
        if (!Files.exists(path)) {
            return;
        }
        try {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    if (log.isDebugEnabled()) {
                        log.debug("> postVisitDirectory, dir=" + dir);
                    }
                    Files.deleteIfExists(dir);
                    return super.postVisitDirectory(dir, exc);
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (log.isDebugEnabled()) {
                        log.debug("> visitFile, file=" + file);
                    }
                    Files.deleteIfExists(file);
                    return super.visitFile(file, attrs);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
