package com.hleofxquotes.md.io;

import java.io.IOException;

public class MdFileOpenException extends IOException {

    public MdFileOpenException(String message, Throwable cause) {
        super(message, cause);
    }

    public MdFileOpenException(Exception e) {
        super(e);
    }

}
