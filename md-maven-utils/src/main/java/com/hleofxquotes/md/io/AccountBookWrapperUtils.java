package com.hleofxquotes.md.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyUtil;
import com.infinitekind.util.StreamTable;
import com.infinitekind.util.StreamVector;
import com.infinitekind.util.StringEncodingException;
import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.apps.md.controller.MDException;
import com.moneydance.apps.md.controller.UserPreferences;
import com.moneydance.apps.md.view.gui.MDStrings;
import com.moneydance.apps.md.view.resources.InitialAccountsLoader;
import com.moneydance.apps.md.view.resources.Resources;
import com.moneydance.security.SecretKeyCallback;

public class AccountBookWrapperUtils {
    private static final Log log = LogFactory.getLog(AccountBookWrapperUtils.class);

    /** The Constant DEFAULT_CURRENCY. */
    private static final String DEFAULT_CURRENCY = "USD";

    private AccountBookWrapperUtils() {
        super();
    }

    /**
     * Creates the account book wrapper.
     *
     * @param dir the dir
     * @return the account book wrapper
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static AccountBookWrapper createWithBasicAccounts(File dir) throws IOException {
        SecretKeyCallback callback = null;
        String primaryCurrency = null;
        boolean basicAccounts = true;

        return AccountBookWrapperUtils.create(dir, callback, primaryCurrency, basicAccounts);
    }

    public static AccountBookWrapper createWithStandardAccounts(File dir) throws IOException {
        SecretKeyCallback callback = null;
        String primaryCurrency = null;
        boolean basicAccounts = false;

        return AccountBookWrapperUtils.create(dir, callback, primaryCurrency, basicAccounts);
    }

    /**
     * Creates the account book wrapper.
     *
     * @param dir             the dir
     * @param callback        the callback
     * @param primaryCurrency the primary currency
     * @return the account book wrapper
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final AccountBookWrapper create(File dir, SecretKeyCallback callback, String primaryCurrency,
            boolean basicAccounts) throws IOException {
        AccountBookWrapper bookWrapper = AccountBookWrapper.createWrapperForNewFolder(dir);
        try {
            if (callback == null) {
                callback = new SecretKeyCallback() {

                    @Override
                    public String getPassphrase(String var1, String var2) {
                        return null;
                    }

                    @Override
                    public String getPassphrase(String var1) {
                        return null;
                    }
                };
            }
            bookWrapper.loadLocalStorage(callback);
            bookWrapper.loadDataModel(callback);

            AccountBook book = bookWrapper.getBook();
            if (primaryCurrency == null) {
                primaryCurrency = DEFAULT_CURRENCY;
            }
            AccountBookWrapperUtils.loadDefaultAccounts(book, primaryCurrency, basicAccounts);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new IOException(e);
        } finally {
            bookWrapper.getBook().save();
        }

        return bookWrapper;
    }

    /**
     * Load default accounts.
     *
     * @param book            the book
     * @param primaryCurrency the primary currency
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final void loadDefaultAccounts(AccountBook book, String primaryCurrency, boolean basicAccounts)
            throws IOException {
        book.setFinishedInitialLoad(false);
        try {
            UserPreferences preferences = UserPreferences.getInstance();
            Resources resources = preferences.getResources();

            CurrencyTable currencyTable = CurrencyUtil.createDefaultTable(book, primaryCurrency);

            String resourceName = "/localized/strings/default_home.accts";
            if (basicAccounts) {
                resourceName = "/localized/strings/default_basic_home.accts";
            }
            StreamTable accountsTable = AccountBookWrapperUtils.getStreamTable(resources, resourceName);
            MDStrings mdStrings = MDStrings.getSingleton();
            StreamTable rootTable = (StreamTable) accountsTable.get("root");
            InitialAccountsLoader.readAccountsFromTable(book, mdStrings, currencyTable, book.getRootAccount(),
                    (StreamVector) rootTable.get("sub_accounts"), currencyTable.getBaseType());
            book.getRootAccount().setAccountName(book.getName());
            book.getRootAccount().setCurrencyType(currencyTable.getBaseType());
            book.saveTrunkFile();
        } catch (Exception e) {
            throw new IOException(e);
        } finally {
            book.setFinishedInitialLoad(true);
        }
    }

    /**
     * Gets the stream table.
     *
     * @param resources    the resources
     * @param resourceName the resource name
     * @return the stream table
     * @throws StringEncodingException the string encoding exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    private static final StreamTable getStreamTable(Resources resources, String resourceName)
            throws StringEncodingException, IOException {
        StreamTable streamTable = new StreamTable();
        InputStream in = null;
        try {
            in = resources.getClass().getResourceAsStream(resourceName);
            streamTable.readFrom(in);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.warn(e);
                } finally {
                    in = null;
                }
            }
        }
        return streamTable;
    }

    /**
     * Open.
     *
     * @param folderName the folder name
     * @param callback   the callback
     * @return the account book wrapper
     * @throws MdFileOpenException
     * @throws Exception           the exception
     */
    public static AccountBookWrapper open(String folderName, SecretKeyCallback callback) throws MdFileOpenException {
        final File folder = new File(folderName);

        final AccountBookWrapper accountBookWrapper = AccountBookWrapper.wrapperForFolder(folder);

        AccountBook book = accountBookWrapper.getBook();

        if (callback == null) {
            callback = new DefaultSecretKeyCallback(accountBookWrapper);
        }
        try {
            accountBookWrapper.loadLocalStorage(callback);
            accountBookWrapper.loadDataModel(callback);
        } catch (MDException e) {
            String msg = e.getMessage();
            if (msg == null) {
                msg = e.getMsgKey();
            }
            throw new MdFileOpenException(msg, e);
        } catch (Exception e) {
            throw new MdFileOpenException(e);
        }

        book.performPostLoadVerification();

        return accountBookWrapper;
    }

    /**
     * Open.
     *
     * @param folderName the folder name
     * @return the account book wrapper
     * @throws MdFileOpenException
     * @throws Exception           the exception
     */
    public static AccountBookWrapper open(String folderName) throws MdFileOpenException {
        SecretKeyCallback callback = null;
        return AccountBookWrapperUtils.open(folderName, callback);
    }

    public static final void deleteFolder(File folder) {
        TempDirectory.delete(folder.toPath());
    }

    public static final AccountBookWrapper openFolder(File folder) throws MdFileOpenException {
        return open(folder.getAbsolutePath());
    }

    public static AccountBookWrapper open(String folderName, final String passPhrase) throws MdFileOpenException {
        // final String secretPassPhrase = passPhrase;
        SecretKeyCallback callback = null;
        AccountBookWrapper accountBookWrapper = null;

        if (!StringUtils.isEmpty(passPhrase)) {
            callback = new SecretKeyCallback() {

                @Override
                public String getPassphrase(String dataName, String hint) {
                    log.info("> getPassphrase, dataName=" + dataName + ", hint=" + hint);
                    return passPhrase;
                }

                @Override
                public String getPassphrase(String dataName) {
                    log.info("> getPassphrase, dataName=" + dataName);

                    return passPhrase;
                }
            };
        }
        
        log.info("secretKeyCallback=" + callback);
        
        if (callback == null) {
            accountBookWrapper = open(folderName);
        } else {
            accountBookWrapper = open(folderName, callback);
        }
        return accountBookWrapper;
    }

}
