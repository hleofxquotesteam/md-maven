package com.hleofxquotes.md.io;

import java.lang.reflect.Field;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.account.AccountUtils;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.moneydance.model.TransactionSet;
import com.moneydance.apps.md.controller.AccountBookWrapper;

// TODO: Auto-generated Javadoc
/**
 * The Class MdFileUtils.
 */
public class MdFileUtils {

    /** The Constant log. */
    private static final Log log = LogFactory.getLog(MdFileUtils.class);

    /**
     * Check book wrapper.
     *
     * @param bookWrapper the book wrapper
     * @param category    the category
     */
    public static final void checkBookWrapper(AccountBookWrapper bookWrapper, boolean category) {
        AccountBook book = bookWrapper.getBook();

        AccountIterator accountIterator = new AccountIterator(book);
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();

            Account.AccountType accountType = account.getAccountType();

            if (!category) {
                if (AccountUtils.isCategory(accountType)) {
                    continue;
                }
            }

            final Account parentAccount = account.getParentAccount();
            log.info("  account=" + account.getFullAccountName() + ", type=" + accountType + ", category="
                    + AccountUtils.isCategory(accountType) + ", parent="
                    + ((parentAccount == null) ? parentAccount : parentAccount.getAccountName()));
        }

        TransactionSet transactionSet = book.getTransactionSet();
        if (transactionSet != null) {
            for (AbstractTxn transaction : transactionSet) {
                Account account = transaction.getAccount();
                Account.AccountType accountType = account.getAccountType();
                if (!category) {
                    if (AccountUtils.isCategory(accountType)) {
                        continue;
                    }
                }
                log.info("  " + transaction.toString());
            }
        }
    }

    /**
     * Check book wrapper.
     *
     * @param wrapper the wrapper
     */
    public static void checkBookWrapper(AccountBookWrapper wrapper) {
        checkBookWrapper(wrapper, true);
    }

    public static final <T, V> V getPrivateFieldValue(T instance, Class<V> fieldClass, String fieldName)
            throws Exception {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        V fieldValue = fieldClass.cast(field.get(instance));
        return fieldValue;
    }
}
