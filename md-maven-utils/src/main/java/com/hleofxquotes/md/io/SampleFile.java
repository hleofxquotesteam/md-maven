package com.hleofxquotes.md.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.account.AccountUtils;
import com.hleofxquotes.md.category.CategoryUtils;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountUtil;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.util.DateUtil;
import com.moneydance.apps.md.controller.AccountBookWrapper;

/**
 * The Class SampleFile.
 */
public class SampleFile {

    /** The Constant log. */
    private static final Log log = LogFactory.getLog(SampleFile.class);

    /** The Constant SAMPLE_FILE. */
    private static final String SAMPLE_FILE = "sampleFile";

    /** The Constant SAMPLE_MONEYDANCE. */
    private static final String SAMPLE_MONEYDANCE = "Sample.moneydance";

    /**
     * Creates the sample file.
     *
     * @param destPath the dest path
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final File createSampleFile(Path destPath) throws IOException {
        // make a copy first
        File jarFile = File.createTempFile("md-", ".jar");
        jarFile.deleteOnExit();
        try (InputStream stream = SampleFile.class.getResourceAsStream(SampleFile.SAMPLE_FILE)) {
            // Assert.assertNotNull(stream);
            CopyOption options = StandardCopyOption.REPLACE_EXISTING;
            Files.copy(stream, jarFile.toPath(), options);
        }
        log.info("Temp jarFile=" + jarFile.getAbsolutePath());

//        String prefix = "md-";
//        TempDirectory tempDirectory = new TempDirectory(prefix);
//        tempDirectory.deleteOnExit();
//        Path destPath = tempDirectory.getPath();
        if (!Files.exists(destPath)) {
            Files.createDirectories(destPath);
        }

        File destDir = destPath.toFile();
//        Files.createDirectories(destDir.toPath());

        log.info("destDir=" + destDir.getAbsolutePath());
        // Assert.assertNotNull(destDir);
        // Assert.assertTrue(destDir.exists());
        // Assert.assertTrue(destDir.isDirectory());

        try (ZipFile archive = new ZipFile(jarFile)) {
            // sort entries by name to always create folders first
            List<? extends ZipEntry> entries = archive.stream().sorted(Comparator.comparing(ZipEntry::getName))
                    .collect(Collectors.toList());

            // copy each entry in the dest path
            for (ZipEntry entry : entries) {
                Path entryDest = destPath.resolve(entry.getName());

                if (entry.isDirectory()) {
                    log.info("created directory=" + entryDest.toFile().getAbsoluteFile());
                    if (!Files.exists(entryDest)) {
                        Files.createDirectories(entryDest);
                    }
                    continue;
                }

                CopyOption options = StandardCopyOption.REPLACE_EXISTING;
                Files.copy(archive.getInputStream(entry), entryDest, options);
            }
        }

        File folder = new File(destDir, SampleFile.SAMPLE_MONEYDANCE);

        return folder;
    }

    public static final File createSampleFolderWithData() throws IOException, MdFileOpenException {
        final String prefix = "md-";
        final TempDirectory tempDirectory = new TempDirectory(prefix);
        tempDirectory.deleteOnExit();
        final Path destPath = tempDirectory.getPath();
        // final Path destPath = new File("/tmp").toPath();

        final File folder = createSampleFile(destPath);
        final AccountBookWrapper accountBookWrapper = AccountBookWrapperUtils.open(folder.getAbsolutePath());
        final AccountBook accountBook = accountBookWrapper.getBook();
//        Assert.assertTrue(accountBook.isValid());

        String accountName = null;

        for (AccountType accountType : Account.AccountType.values()) {
            if ((accountType == AccountType.ROOT) || (accountType == AccountType.SECURITY)) {
                continue;
            }
            accountName = accountType + "-" + UUID.randomUUID().toString();
            Account account = AccountUtils.createAccount(accountBook, accountName, accountType);
            if (account != null) {
                account.ensureAccountStructure();
                account.syncItem();
            } else {
                log.warn("Cannot create account for accountType=" + accountType);
            }
        }

        Account rootAccount = accountBook.getRootAccount();
//        Assert.assertNotNull(rootAccount);

        for (Account account : rootAccount.getSubAccounts()) {
//            Assert.assertNotNull(account);
            AccountType accountType = account.getAccountType();
//            Assert.assertNotNull(accountType);

            if (accountType == AccountType.BANK) {
                addTransaction(accountBook, account);
            }
        }

        accountBookWrapper.getBook().save();
        return folder;
    }

    /**
     * Adds the transaction.
     *
     * @param accountBook the account book
     * @param account     the account
     */
    private static final void addTransaction(final AccountBook accountBook, Account account) {
        AccountBook book = accountBook;
        int date = DateUtil.getStrippedDateInt();
        int taxDate = date;
        long dateEntered = System.currentTimeMillis();
        String checkNumber = "";
        String description = "description - Created for testing";
        String memo = "memo - Created for testing";
        long id = -1L;
        byte status = ParentTxn.STATUS_UNRECONCILED;
        ParentTxn parentTxn = ParentTxn.makeParentTxn(book, date, taxDate, dateEntered, checkNumber, account,
                description, memo, id, status);
        parentTxn.setIsNew(true);
        parentTxn.syncItem();

        long parentAmount = 100L;
        long splitAmount = parentAmount;
        double rate = 1.0;
        Account category = AccountUtil.getDefaultCategoryForAcct(account);
        category = CategoryUtils.getCategory(book.getRootAccount(), "Bills:Phone");
        log.info("NEW transaction.category=" + category);
        long txnId = -1L;
        SplitTxn splitTxn = SplitTxn.makeSplitTxn(parentTxn, parentAmount, splitAmount, rate, category, description,
                txnId, status);
        splitTxn.setIsNew(true);
        splitTxn.syncItem();

        parentTxn.addSplit(splitTxn);

        book.getTransactionSet().addNewTxn(parentTxn);
        book.refreshAccountBalances();
    }

}
