package com.hleofxquotes.md.io;

import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.security.SecretKeyCallback;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultSecretKeyCallback.
 */
final class DefaultSecretKeyCallback implements SecretKeyCallback {

    /** The account book wrapper. */
    private final AccountBookWrapper accountBookWrapper;

    /**
     * Instantiates a new default secret key callback.
     *
     * @param accountBookWrapper the account book wrapper
     */
    DefaultSecretKeyCallback(AccountBookWrapper accountBookWrapper) {
        this.accountBookWrapper = accountBookWrapper;
    }

    /**
     * Gets the passphrase.
     *
     * @param hint the hint
     * @return the passphrase
     */
    public String getPassphrase(String hint) {
        return accountBookWrapper.getEncryptionKey();
    }

    /**
     * Gets the passphrase.
     *
     * @param dataName the data name
     * @param hint     the hint
     * @return the passphrase
     */
    public String getPassphrase(String dataName, String hint) {
        return accountBookWrapper.getEncryptionKey();
    }
}