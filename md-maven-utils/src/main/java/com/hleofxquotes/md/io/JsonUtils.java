package com.hleofxquotes.md.io;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.stream.JsonWriter;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.LocalStorage;
import com.infinitekind.moneydance.model.MoneydanceSyncableItem;
import com.infinitekind.tiksync.SyncableItem;
import com.infinitekind.util.DateUtil;
import com.infinitekind.util.StreamTable;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class JsonUtils {
    private static final String DEFAULT_INDENT = "  ";
    public static final String TAG_LOCAL_STORAGE = "localStorage";
    public static final String TAG_SYNCABLE_ITEMS = "syncableItems";
    public static final String TAG_METADATA = "metadata";

    public static final void writeAccountBookWrapper(AccountBookWrapper wrapper, Writer writer) throws IOException {
        AccountBook book = wrapper.getBook();
        JsonWriter jsonWriter = new JsonWriter(writer);
        jsonWriter.setIndent(DEFAULT_INDENT);

        jsonWriter.beginObject();
        try {
            // node - metadata
            JsonUtils.writeMetadata(TAG_METADATA, book, jsonWriter);

            // node - all_items
            List<SyncableItem> syncableItems = book.getSyncer().getSyncedDocument().allItems();
            JsonUtils.writeSyncableItems(TAG_SYNCABLE_ITEMS, syncableItems, jsonWriter);

            // node - local_settings
            LocalStorage localStorage = book.getLocalStorage();
            JsonUtils.writeMap(TAG_LOCAL_STORAGE, localStorage, jsonWriter);
        } finally {
            jsonWriter.endObject();
            jsonWriter.close();
        }
    }

    public static void writeSyncableItems(String tag, List<SyncableItem> items, JsonWriter jsonWriter)
            throws IOException {
        jsonWriter.name(tag);

        JsonUtils.writeSyncableItems(items, jsonWriter);
    }

    public static void writeSyncableItems(List<SyncableItem> items, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginArray();
        try {
            for (SyncableItem item : items) {
                JsonUtils.writeSyncableItem(item, jsonWriter);
            }
        } finally {
            jsonWriter.endArray();
        }
    }

    public static <T extends SyncableItem> void writeSyncableItem(T item, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginObject();

        try {
            jsonWriter.name("syncItemType");
            jsonWriter.value(item.getSyncItemType());

            jsonWriter.name("syncTimestamp");
            jsonWriter.value(item.getSyncTimestamp());

            jsonWriter.name("uuid");
            jsonWriter.value(item.getUUID());

            if (item instanceof MoneydanceSyncableItem) {
                MoneydanceSyncableItem moneydanceSyncableItem = (MoneydanceSyncableItem) item;
                writeMoneydanceSyncableItem(moneydanceSyncableItem, jsonWriter);
            }
        } finally {
            jsonWriter.endObject();
        }
    }

    private static void writeMoneydanceSyncableItem(MoneydanceSyncableItem moneydanceSyncableItem,
            JsonWriter jsonWriter) throws IOException {
        Set<String> keys = moneydanceSyncableItem.getParameterKeys();
        for (String key : keys) {
            jsonWriter.name(key);
            String value = moneydanceSyncableItem.getParameter(key, "");
            jsonWriter.value(value);
        }
    }

    public static <T extends Map<K, V>, K, V> void writeMap(String tag, T map, JsonWriter jsonWriter)
            throws IOException {
        jsonWriter.name(tag);

        final Set<K> keys = map.keySet();
        jsonWriter.beginObject();
        try {
            for (K key : keys) {
                jsonWriter.name(key.toString());
                jsonWriter.value(map.get(key).toString());
            }
        } finally {
            jsonWriter.endObject();
        }
    }

    public static void writeMetadata(String tag, AccountBook book, JsonWriter jsonWriter) throws IOException {
        jsonWriter.name(tag);

        jsonWriter.beginObject();
        try {
//            String exporter = "Moneydance 2021 (3030)";
//            jsonWriter.name("exporter").value(exporter);

//        jsonWriter.name("moneydance_build").value((long) this.mdGUI.getMain().getBuild());
            jsonWriter.name("exportDate").value((long) DateUtil.getStrippedDateInt());
            jsonWriter.name("fileName").value(book.getName());
            jsonWriter.name("filePath").value(book.getRootFolder().getAbsolutePath());

            StreamTable publicMetaData = book.getPublicMetaData();
            writeStreamTable("publicMetaData", publicMetaData, jsonWriter);

//        jsonWriter.name("extensions").beginArray();
//        FeatureModule[] var5 = this.mdGUI.getMain().getLoadedModules();
//        int var6 = var5.length;

//        for (int var7 = 0; var7 < var6; ++var7) {
//            FeatureModule extension = var5[var7];
//            jsonWriter.beginObject();
//            jsonWriter.name("id").value(extension.getIDStr());
//            jsonWriter.name("build").value((long) extension.getBuild());
//            jsonWriter.endObject();
//        }
//        jsonWriter.endArray();
        } finally {
            jsonWriter.endObject();
        }
    }

    private static <T extends StreamTable> void writeStreamTable(String tag, T streamTable, JsonWriter jsonWriter)
            throws IOException {
        jsonWriter.name(tag);

        jsonWriter.beginObject();
        try {
            Set keySet = streamTable.keySet();
            for (Object key : keySet) {
                Object value = streamTable.get(key);
                jsonWriter.name(key.toString());
                if (value == null) {
                    jsonWriter.value("");
                } else {
                    jsonWriter.value(value.toString());
                }
            }
        } finally {
            jsonWriter.endObject();
        }
    }
}
