package com.hleofxquotes.md.io;

import java.awt.Image;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.apps.md.controller.FeatureModule;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.extensionapi.AccountEditor;
import com.moneydance.apps.md.view.HomePageView;

public class DefaultFeatureModuleContext implements FeatureModuleContext {
    private static final Log LOGGER = LogFactory.getLog(DefaultFeatureModuleContext.class);

    private AccountBookWrapper accountBookWrapper;

    public DefaultFeatureModuleContext(AccountBookWrapper accountBookWrapper) {
        this.accountBookWrapper = accountBookWrapper;
    }

    @Override
    public void showURL(String arg0) {
        LOGGER.info("> showURL");

    }

    @Override
    public void registerHomePageView(FeatureModule arg0, HomePageView arg1) {
        LOGGER.info("> registerHomePageView");
    }

    @Override
    public void registerFeature(FeatureModule arg0, String arg1, Image arg2, String arg3) {
        LOGGER.info("> registerFeature");
    }

    @Override
    public void registerAccountEditor(FeatureModule arg0, int arg1, AccountEditor arg2) {
        LOGGER.info("> registerAccountEditor");
    }

    @Override
    public String getVersion() {
        LOGGER.info("> getVersion");
        return "1.0.0";
    }

    @Override
    public Account getRootAccount() {
        LOGGER.info("> getRootAccount");
        return this.accountBookWrapper.getBook().getRootAccount();
    }

    @Override
    public AccountBook getCurrentAccountBook() {
        LOGGER.info("> getCurrentAccountBook");
        return this.accountBookWrapper.getBook();
    }

    @Override
    public int getBuild() {
        LOGGER.info("> getBuild");
        return 1;
    }
}