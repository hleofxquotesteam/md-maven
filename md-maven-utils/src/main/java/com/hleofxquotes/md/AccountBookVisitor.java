package com.hleofxquotes.md;

import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.moneydance.model.TransactionSet;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public interface AccountBookVisitor {

    void visit();

    void preVisitAccountBookWrapper(AccountBookWrapper accountBookWrapper);

    void visitAccountBookWrapper(AccountBookWrapper accountBookWrapper);

    void postVisitAccountBookWrapper(AccountBookWrapper accountBookWrapper);

    void preVisitRoot(Account account);

    void visitRoot(Account account);

    void postVisitRoot(Account account);

    void preVisitAccount(Account account);

    void visitAccount(Account account);

    void postVisitAccount(Account account);

    void preVisitSecurity(Account account);

    void visitSecurity(Account account);

    void postVisitSecurity(Account account);

    void preVisitCategory(Account account);

    void visitCategory(Account account);

    void postVisitCategory(Account account);

    void preVisitAccountUnknown(Account account);

    void visitAccountUnknown(Account account);

    void postVisitAccountUnknown(Account account);

    void preVisitParentTxn(ParentTxn parentTxn);

    void visitParentTxn(ParentTxn parentTxn);

    void postVisitParentTxn(ParentTxn parentTxn);

    void preVisitUnknownTxn(AbstractTxn transaction);

    void visitUnknownTxn(AbstractTxn transaction);

    void postVisitUnknownTxn(AbstractTxn transaction);

    void preVisitSplitTxn(SplitTxn splitTxn);

    void visitSplitTxn(SplitTxn splitTxn);

    void postVisitSplitTxn(SplitTxn splitTxn);

    void preVisitMdAccount(Account account);

    void visitMdAccount(Account account);

    void postVisitMdAccount(Account account);

    void preVisitTransactionSet(TransactionSet transactions);

    void visitTransactionSet(TransactionSet transactions);

    void postVisitTransactionSet(TransactionSet transactions);

}
