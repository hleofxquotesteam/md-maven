package com.hleofxquotes.md;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.infinitekind.util.IOUtils;

public class BackupUtils {
    private static final Log LOGGER = LogFactory.getLog(BackupUtils.class);

    public static File expandArchiveFile(File archiveFile) {
        File tempFolder = IOUtils.createTempFolder();
        // tempFolder.deleteOnExit();
    
        LOGGER.info("Created tempDir=" + tempFolder.getAbsolutePath());
    
        return expandArchiveFile(archiveFile, tempFolder);
    }

    private static final File expandArchiveFile(File archiveFile, File dest) {
        IOUtils.openZip(archiveFile, dest.getAbsolutePath());
        String[] zipContents = dest.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(MdMainFrame.SUFFIX_MONEYDANCE);
            }
        });
        if (zipContents == null || zipContents.length <= 0) {
            return null;
        }
    
        File tmpMDFile = new File(dest, zipContents[0]);
        File moneydanceDirectory = tmpMDFile;
        // File newBookFile =
        // AccountBook.getUnusedFileNameWithBase(AccountBookUtil.DEFAULT_FOLDER_CONTAINER,
        // baseFilename);
        // if (!tmpMDFile.renameTo(newBookFile)) {
        // try {
        // IOUtils.copyFolder(tmpMDFile, newBookFile);
        // } catch (IOException var13) {
        // this.reportError(this.getStr("move_internal_storage_failed"), var13);
        // return false;
        // }
        // }
    
        // AccountBookWrapper book = AccountBookWrapper.wrapperForFolder(newBookFile);
        // if (book != null) {
        // book.setUUIDResetFlag(true);
        // }
    
        return moneydanceDirectory;
    }

    public static final void deleteDirectoryRecursively(Path pathToBeDeleted) throws IOException {
        LOGGER.info("Deleting directory=" + pathToBeDeleted.toString());

        Files.walk(pathToBeDeleted).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
    }

}
