package com.hleofxquotes.md.account;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.Vector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.category.CategoryUtils;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;
import com.infinitekind.moneydance.model.Legacy;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountUtils.
 */
public class AccountUtils {

    /** The Constant log. */
    private static final Log log = LogFactory.getLog(AccountUtils.class);

    /**
     * Creates the account.
     *
     * @param book        the book
     * @param accountName the account name
     * @param accountType the account type
     * @return the account
     */
    public static Account createAccount(AccountBook book, String accountName, Account.AccountType accountType) {
        Account newAccount = null;

        CurrencyTable currencyTable = book.getCurrencies();
        CurrencyType currencyType = currencyTable.getBaseType();

        Account parentAccount = book.getRootAccount();

        switch (accountType) {
        case EXPENSE:
            newAccount = createExpenseAccount(book, accountName, currencyType, parentAccount);
            break;
        case INCOME:
            newAccount = createIncomeAccount(book, accountName, currencyType, parentAccount);
            break;
        case ROOT:
            log.warn("Cannot create ROOT account.");
            break;
        case BANK:
            newAccount = createBankAccount(book, accountName, currencyType, parentAccount);
            break;
        case INVESTMENT:
            newAccount = createInvestmentAccount(book, accountName, currencyType, parentAccount);
            break;
        case CREDIT_CARD:
            newAccount = createCreditCardAccount(book, accountName, currencyType, parentAccount);
            break;
        case SECURITY:
            log.warn("Cannot create SECURITY account.");
            break;
        case ASSET:
            newAccount = createAssetAccount(book, accountName, currencyType, parentAccount);
            break;
        case LIABILITY:
            newAccount = createLiabilityAccount(book, accountName, currencyType, parentAccount);
            break;
        case LOAN:
            newAccount = createLoanAccount(book, accountName, currencyType, parentAccount);
            break;
        default:
            log.warn("Don't know how to create accountType" + accountType);
            break;
        }

        if (newAccount != null) {
            newAccount.ensureAccountStructure();
            newAccount.syncItem();
        }

        return newAccount;
    }

    /**
     * Creates the loan account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createLoanAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.LOAN;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @param accountType   the account type
     * @return the account
     */
    private static Account createAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount, Account.AccountType accountType) {
        Account newAccount;

        final int accountID = -1;

        final Hashtable accountInfo = null;
        final Vector subAccounts = null;
        final long startBalance = 0L;

        newAccount = Legacy.makeAccount(book, accountName, accountID, accountType, currencyType, accountInfo,
                subAccounts, parentAccount, startBalance);

        return newAccount;
    }

    /**
     * Creates the liability account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createLiabilityAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.LIABILITY;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the asset account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createAssetAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.ASSET;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the credit card account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createCreditCardAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.CREDIT_CARD;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the investment account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createInvestmentAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.INVESTMENT;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the bank account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createBankAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.BANK;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the income account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createIncomeAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.INCOME;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    /**
     * Creates the expense account.
     *
     * @param book          the book
     * @param accountName   the account name
     * @param currencyType  the currency type
     * @param parentAccount the parent account
     * @return the account
     */
    private static Account createExpenseAccount(AccountBook book, String accountName, CurrencyType currencyType,
            Account parentAccount) {
        final Account.AccountType accountType = Account.AccountType.EXPENSE;

        return createAccount(book, accountName, currencyType, parentAccount, accountType);
    }

    public static boolean isRoot(Account account) {
        return isRoot(account.getAccountType());
    }

    /**
     * Checks if is root.
     *
     * @param accountType the account type
     * @return true, if is root
     */
    public static boolean isRoot(Account.AccountType accountType) {
        return accountType == Account.AccountType.ROOT;
    }

    public static boolean isSecurity(Account account) {
        return isSecurity(account.getAccountType());
    }

    /**
     * Checks if is security.
     *
     * @param accountType the account type
     * @return true, if is security
     */
    public static boolean isSecurity(Account.AccountType accountType) {
        return accountType == Account.AccountType.SECURITY;
    }

    public static boolean isCategory(Account account) {
        return isCategory(account.getAccountType());
    }

    /**
     * Checks if is category.
     *
     * @param accountType the account type
     * @return true, if is category
     */
    public static boolean isCategory(Account.AccountType accountType) {
        boolean category = false;

        switch (accountType) {
        case EXPENSE:
            category = true;
            break;
        case INCOME:
            category = true;
            break;
        default:
            category = false;
            break;
        }

        return category;
    }

    public static boolean isAccount(Account account) {
        return isAccount(account.getAccountType());
    }

    /**
     * Checks if is account.
     *
     * @param accountType the account type
     * @return true, if is account
     */
    public static boolean isAccount(Account.AccountType accountType) {
        boolean account = false;

        switch (accountType) {
        case ASSET:
            account = true;
            break;
        case BANK:
            account = true;
            break;
        case CREDIT_CARD:
            account = true;
            break;
        case EXPENSE:
            account = false;
            break;
        case INCOME:
            account = false;
            break;
        case INVESTMENT:
            account = true;
            break;
        case LIABILITY:
            account = true;
            break;
        case LOAN:
            account = true;
            break;
        case ROOT:
            account = false;
            break;
        case SECURITY:
            account = false;
            break;
        }

        return account;
    }

    public static String getHierarchicalName(Account account) {
        return CategoryUtils.getHierarchicalName(account);
    }

    /**
     * Creates the category.
     *
     * @param parent the parent account
     * @param name   the account name
     * @return the account
     */
    public static Account createAccount(Account parent, String name) {
        Account.AccountType parentType = parent.getAccountType();

        Account account;
        final AccountBook book = parent.getBook();
        final CurrencyType currencyType = parent.getCurrencyType();
        switch (parentType) {
        case INCOME:
            account = Legacy.makeIncomeAccount(book, name, -1, currencyType, null, null, parent);
            break;
        case BANK:
            account = Legacy.makeBankAccount(book, name, -1, currencyType, null, null, parent, 0);
            break;
        case CREDIT_CARD:
            account = Legacy.makeAccount(book, Account.AccountType.CREDIT_CARD, name, currencyType, parent);
            break;
        case ROOT:
        case EXPENSE:
        default:
            account = Legacy.makeExpenseAccount(book, name, -1, currencyType, null, null, parent);
            break;
        }

        account.syncItem();

        return account;
    }

    public static final Stream<Account> getAccountStream(AccountBook book) {
        AccountIterator iterator = new AccountIterator(book);
        Spliterator<Account> spliterator = Spliterators.spliteratorUnknownSize(iterator, 0);
        return StreamSupport.stream(spliterator, false);
    }

    public static final Stream<CurrencyType> getCurrencyTypeStream(AccountBook book) {
        CurrencyTable currencies = book.getCurrencies();

        Iterator<CurrencyType> iterator = currencies.iterator();
        Spliterator<CurrencyType> spliterator = Spliterators.spliteratorUnknownSize(iterator, 0);
        return StreamSupport.stream(spliterator, false);
    }
}
