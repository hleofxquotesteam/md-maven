package com.hleofxquotes.md;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.account.AccountUtils;
import com.hleofxquotes.md.io.AccountBookWrapperUtils;
import com.hleofxquotes.md.io.MdFileOpenException;
import com.hleofxquotes.md.transaction.TransactionUtils;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountIterator;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.moneydance.model.TransactionSet;
import com.moneydance.apps.md.controller.AccountBookWrapper;

public class AbstractAccountBookVisitor implements AccountBookVisitor {
    private static final Log log = LogFactory.getLog(AbstractAccountBookVisitor.class);

    private final AccountBookWrapper accountBookWrapper;

    public AbstractAccountBookVisitor(String folderName, String password) throws MdFileOpenException {
        super();

        if (StringUtils.isNotEmpty(password)) {
            accountBookWrapper = AccountBookWrapperUtils.open(folderName, password);
        } else {
            accountBookWrapper = AccountBookWrapperUtils.open(folderName);
        }
    }

    @Override
    public void visit() {
        preVisitAccountBookWrapper(this.accountBookWrapper);
        try {
            visitAccountBookWrapper(this.accountBookWrapper);
        } finally {
            postVisitAccountBookWrapper(this.accountBookWrapper);
        }
    }

    @Override
    public void preVisitAccountBookWrapper(AccountBookWrapper accountBookWrapper) {
    }

    @Override
    public void visitAccountBookWrapper(AccountBookWrapper accountBookWrapper) {
        AccountIterator accountIterator = new AccountIterator(accountBookWrapper.getBook());
        while (accountIterator.hasNext()) {
            Account account = accountIterator.next();
            preVisitMdAccount(account);
            try {
                visitMdAccount(account);
            } finally {
                postVisitMdAccount(account);
            }
        }

        TransactionSet transactions = accountBookWrapper.getBook().getTransactionSet();
        preVisitTransactionSet(transactions);
        try {
            visitTransactionSet(transactions);
        } finally {
            postVisitTransactionSet(transactions);
        }
    }

    @Override
    public void postVisitAccountBookWrapper(AccountBookWrapper accountBookWrapper) {

    }

    @Override
    public void visitTransactionSet(TransactionSet transactions) {
        for (AbstractTxn transaction : transactions) {
            Account account = transaction.getAccount();
            if (TransactionUtils.isParent(transaction)) {
                if (account != null) {
                    ParentTxn parentTxn = (ParentTxn) transaction;
                    preVisitParentTxn(parentTxn);
                    try {
                        visitParentTxn(parentTxn);
                    } finally {
                        postVisitParentTxn(parentTxn);
                    }
                }
            } else if (TransactionUtils.isSplit(transaction)) {
                if (account != null) {
                    SplitTxn splitTxn = (SplitTxn) transaction;
                    preVisitSplitTxn(splitTxn);
                    try {
                        visitSplitTxn(splitTxn);
                    } finally {
                        postVisitSplitTxn(splitTxn);
                    }
                }
            } else {
                preVisitUnknownTxn(transaction);
                try {
                    visitUnknownTxn(transaction);
                } finally {
                    postVisitUnknownTxn(transaction);
                }
            }
        }
    }

    @Override
    public void preVisitMdAccount(Account account) {

    }

    @Override
    public void visitMdAccount(Account account) {
        if (AccountUtils.isRoot(account)) {
            prePostVisitRoot(account);
        } else if (AccountUtils.isAccount(account)) {
            prePostVisitAccount(account);
        } else if (AccountUtils.isSecurity(account)) {
            prePostVisitSecurity(account);
        } else if (AccountUtils.isCategory(account)) {
            prePostVisitCategory(account);
        } else {
            prePostVisitAccountUnknown(account);
        }
    }

    protected void prePostVisitAccountUnknown(Account account) {
        preVisitAccountUnknown(account);
        try {
            visitAccountUnknown(account);
        } finally {
            postVisitAccountUnknown(account);
        }
    }

    protected void prePostVisitCategory(Account account) {
        preVisitCategory(account);
        try {
            visitCategory(account);
        } finally {
            postVisitCategory(account);
        }
    }

    protected void prePostVisitSecurity(Account account) {
        preVisitSecurity(account);
        try {
            visitSecurity(account);
        } finally {
            postVisitSecurity(account);
        }
    }

    protected void prePostVisitAccount(Account account) {
        preVisitAccount(account);
        try {
            visitAccount(account);
        } finally {
            postVisitAccount(account);
        }
    }

    protected void prePostVisitRoot(Account account) {
        preVisitRoot(account);
        try {
            visitRoot(account);
        } finally {
            postVisitRoot(account);
        }
    }

    @Override
    public void postVisitMdAccount(Account account) {

    }

    @Override
    public void preVisitRoot(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitRoot(Account account) {
        log.info("ROOT - root=" + account + ", type=" + account.getAccountType());
    }

    @Override
    public void postVisitRoot(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitAccount(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitAccount(Account account) {
        log.info("account=" + account + ", type=" + account.getAccountType());

        for (Account subAccount : account.getSubAccounts()) {
            preVisitMdAccount(subAccount);
            try {
                visitMdAccount(subAccount);
            } finally {
                postVisitMdAccount(subAccount);
            }
        }
    }

    @Override
    public void postVisitAccount(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitSecurity(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitSecurity(Account account) {
        log.info("security=" + account + ", type=" + account.getAccountType());
    }

    @Override
    public void postVisitSecurity(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitCategory(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitCategory(Account account) {
        log.info("category=" + account + ", type=" + account.getAccountType());
    }

    @Override
    public void postVisitCategory(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitAccountUnknown(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitAccountUnknown(Account account) {
        log.info("unknown=" + account + ", type=" + account.getAccountType());

    }

    @Override
    public void postVisitAccountUnknown(Account account) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitParentTxn(ParentTxn parentTxn) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitParentTxn(ParentTxn parentTxn) {
        if (parentTxn != null) {
            Account category = parentTxn.getAccount();
            ParentTxn parentParentTxn = parentTxn.getParentTxn();

            log.info("ParentTxn " + "category=" + category + ", description=" + parentParentTxn.getDescription()
                    + ", memo=" + parentParentTxn.getMemo() + ", uuid=" + parentTxn.getUUID() + ", parent="
                    + (parentParentTxn == null ? parentParentTxn : parentParentTxn.getUUID()));

            int splitCount = parentTxn.getSplitCount();
            for (int i = 0; i < splitCount; i++) {
                SplitTxn splitTxn = parentTxn.getSplit(i);
                preVisitSplitTxn(splitTxn);
                try {
                    visitSplitTxn(splitTxn);
                } finally {
                    postVisitSplitTxn(splitTxn);
                }
            }
        }
    }

    @Override
    public void postVisitParentTxn(ParentTxn parentTxn) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitUnknownTxn(AbstractTxn transaction) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitUnknownTxn(AbstractTxn transaction) {
        log.error("Unknown transaction class=" + transaction.getClass().getName());
    }

    @Override
    public void postVisitUnknownTxn(AbstractTxn transaction) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitSplitTxn(SplitTxn splitTxn) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitSplitTxn(SplitTxn splitTxn) {
        if (splitTxn != null) {
            Account category = splitTxn.getAccount();
            ParentTxn parentTxn = splitTxn.getParentTxn();
            log.info("  SplitTxn category=" + category + ", amount=" + splitTxn.getAmount() + ", description="
                    + splitTxn.getDescription() + ", uuid=" + splitTxn.getUUID() + ", parent="
                    + (parentTxn == null ? parentTxn : parentTxn.getUUID()));
        }
    }

    @Override
    public void postVisitSplitTxn(SplitTxn splitTxn) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preVisitTransactionSet(TransactionSet transactions) {
        // TODO Auto-generated method stub

    }

    @Override
    public void postVisitTransactionSet(TransactionSet transactions) {
        // TODO Auto-generated method stub

    }

}