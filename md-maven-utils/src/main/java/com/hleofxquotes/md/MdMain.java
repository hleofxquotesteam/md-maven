package com.hleofxquotes.md;

import com.moneydance.apps.md.controller.Main;

// TODO: Auto-generated Javadoc
/**
 * The Class MdMain.
 */
public class MdMain extends com.moneydance.apps.md.controller.Main {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        Main.DEBUG = true;
        com.moneydance.apps.md.controller.Main.main(args);
    }

}
