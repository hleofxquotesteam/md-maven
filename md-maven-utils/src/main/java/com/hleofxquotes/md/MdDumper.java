package com.hleofxquotes.md;

import javax.crypto.SecretKey;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hleofxquotes.md.io.MdFileOpenException;
import com.hleofxquotes.md.io.MdFileUtils;
import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.apps.md.controller.LocalStorageCipher;
import com.moneydance.apps.md.controller.LocalStorageCipher.MDCipherLevel;

public class MdDumper extends AbstractAccountBookVisitor {
    private static final Log log = LogFactory.getLog(MdDumper.class);

    public MdDumper(String folderName, String password) throws MdFileOpenException {
        super(folderName, password);
    }

    public static void main(String[] args) {
        String folderName = null;
        String password = null;

        if (args.length == 1) {
            folderName = args[0];
            password = null;
        } else if (args.length == 2) {
            folderName = args[0];
            password = args[1];
        } else {
            Class<MdDumper> clz = MdDumper.class;
            System.out.println("Usage: java " + clz.getName() + " folder.moneydance [password]");
            System.exit(0);
        }

        try {

            MdDumper mcDumper = new MdDumper(folderName, password);
            mcDumper.visit();

//            logEncryptionInfo(accountBookWrapper);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    private static void logEncryptionInfo(AccountBookWrapper accountBookWrapper) throws Exception {
        SecretKey key = MdFileUtils.getPrivateFieldValue(accountBookWrapper, SecretKey.class, "key");
        log.info("key.getAlgorithm=" + key.getAlgorithm());
        log.info("key.getFormat=" + key.getFormat());
        log.info("key.class=" + key.getClass());

        LocalStorageCipher localStorageCipher = MdFileUtils.getPrivateFieldValue(accountBookWrapper,
                LocalStorageCipher.class, "cipher");
        log.info("cipher=" + localStorageCipher);

        MDCipherLevel cipherLevel = MdFileUtils.getPrivateFieldValue(localStorageCipher, MDCipherLevel.class,
                "cipherLevel");
        log.info("cipher.cipherLevel=" + cipherLevel.toString());
        // final int pbeIterationCount;
        // final int keyLength;
        log.info("cipher.cipherLevel.pbeIterationCount="
                + MdFileUtils.getPrivateFieldValue(cipherLevel, Integer.class, "pbeIterationCount"));
        log.info("cipher.cipherLevel.keyLength="
                + MdFileUtils.getPrivateFieldValue(cipherLevel, Integer.class, "keyLength"));
    }

}
